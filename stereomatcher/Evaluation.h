/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef EVALUATION_H_
#define EVALUATION_H_

#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>

#include "DisparitySearchRange.h"
#include "DisparitySpaceImage.h"
#include "Image.h"
#include "Parameters.h"

class Evaluation {
private:
	const Parameters& params;
	cv::Mat_<float> groundTruthImg;
	std::vector<cv::Mat_<uint8_t> > maskImages;
	double offset, scale;
	bool flagMiddleburyStyle; //whether ground truth and mask image are in Middlebury style
	std::ofstream logFile;
public:
	Evaluation(const Parameters& p);
	~Evaluation();
	void evaluate(DisparitySpaceImage& dsi);
	void evaluate(const DisparitySearchRange& range) const;
	cv::Mat_<float> getGroundTruthImg() const;
	cv::Mat_<uint8_t> getMaskForValidPixels() const;
	void writeDiffToGroundTruthImg(const cv::Mat_<float>& disparityImage,
			const cv::Mat_<uint8_t>& mask, const std::string& filename) const;
	void writeMeanDeviation(const cv::Mat_<float>& disparityImage,
			const cv::Mat_<uint8_t>& mask, const std::string& symbol);
	void writePercentageOfBadPixels(const cv::Mat_<float>& disparityImage,
			const cv::Mat_<uint8_t>& mask, const std::string& symbol,
			const std::string& description);
	void writeAsPercentage(double percentage, const std::string& symbol,
			const std::string& description);
	void writeMiddleburyDispImg(cv::Mat_<float> floatWTAImg);
private:
	void readGroundTruthImg();
	void extractOffsetAndScaleFromFilename(const std::string& fileName);
	void readMasks();
	void writeImg(const std::string& filename, const cv::Mat& img) const;
};

#endif /* EVALUATION_H_ */
