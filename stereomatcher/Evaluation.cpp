/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <cfloat>
#include <iostream>
#include <vector>

#include "Error.h"
#include "Evaluation.h"

Evaluation::Evaluation(const Parameters& p) :
		params(p), flagMiddleburyStyle(false) {
	readGroundTruthImg();
	readMasks();

	logFile.open("errors.log");
	if (logFile.fail())
		Error("*** Could not open the file for logging the error rates! ***");
}

Evaluation::~Evaluation() {
	logFile.close();
}

void Evaluation::evaluate(DisparitySpaceImage& dsi) {
	const cv::Mat_<float>& disparityImage = dsi.getFloatWTAImage();
	cv::Mat_<uint8_t> computedPixels = dsi.getComputedPixelsMat();
	if (params.verbosity & writeBasicConsoleOutput)
		std::cout << "*** Evaluation Nr.: " << params.counter << std::endl;
	if (params.maskFileNames.size() == 0) {
		if (params.verbosity & writeBasicConsoleOutput)
			std::cout << "*** Whole image without using a mask: " << std::endl;
		std::stringstream str;
		str << "wrongPercentage-" << params.counter;
		writePercentageOfBadPixels(disparityImage, maskImages.at(0), str.str(),
				"Percentage of wrongly calculated disparities");
		str.str("");
		str << "meanDeviation-" << params.counter << ".png";
		writeMeanDeviation (disparityImage, maskImages.at(0), str.str());
		if (params.verbosity & (writeImportantImages | writeDebugImages)) {
			str.str("");
			str << "diffToGroundTruth-" << params.counter << ".png";
			writeDiffToGroundTruthImg(disparityImage, maskImages.at(0), str.str());
		}
	} else {
		for (size_t i = 0; i < params.maskFileNames.size(); ++i) {
			std::string filename = params.maskFileNames.at(i);
			size_t found = filename.rfind('/');
			if (found < filename.size() - 1)
				filename = filename.substr(found + 1);

			if (params.verbosity & writeBasicConsoleOutput)
				std::cout << "*** Mask: " << filename << std::endl;

			std::stringstream str;
			str << "wrongPercentageUsingMask-" << filename << "-" << params.counter;
			writePercentageOfBadPixels(disparityImage,
					maskImages.at(i),
					str.str(),
					"Percentage of wrongly calculated disparities");
			str.str("");
			str << "meanDeviationUsingMask-" << filename << "-" << params.counter;
			writeMeanDeviation (disparityImage,
					maskImages.at(i),
					str.str());
			if (params.verbosity & (writeImportantImages | writeDebugImages)) {
				str.str("");
				str << "diffToGroundTruth-" << filename << "-" << params.counter;
				writeDiffToGroundTruthImg(disparityImage, maskImages.at(i), str.str());
			}
		}
	}
}

void Evaluation::evaluate(const DisparitySearchRange& range) const {
	if (params.verbosity & writeDebugImages) {
		if (params.counter == 0)
			range.writeFeaturePoints("debug-searchrange-features.png", getGroundTruthImg());

		cv::Mat_<float> diff;
		cv::Mat_<uint8_t> mask;
		cv::Mat_<uint8_t> red, green, blue;
		cv::Mat_<cv::Vec3b> pngImg;
		const cv::Mat_<int16_t>& minDisp = range.getMinDisp();
		const cv::Mat_<int16_t>& maxDisp = range.getMaxDisp();

		std::stringstream str;
		str << "debug-searchrange-minDisp-" << params.counter << ".png";
		writeImg(str.str(), minDisp);
		str.str("");
		str << "debug-searchrange-maxDisp-" << params.counter << ".png";
		writeImg(str.str(), maxDisp);
		cv::Mat_<int16_t> depth = maxDisp - minDisp;
		str.str("");
		str << "debug-searchrange-range-" << params.counter << ".png";
		writeImg(str.str(), depth);

		for (size_t i = 0; i < maskImages.size(); ++i) {
			// set all regions that contain the right disparity value to green
			cv::normalize(groundTruthImg, green, 0, 255, cv::NORM_MINMAX, CV_8U);
			//set all regions that do not contain the right disparity value to red
			red = cv::Mat::zeros(green.size(), green.type());
			cv::subtract(minDisp, groundTruthImg, diff, cv::noArray(), CV_32F);
			mask = diff > 0;
			red.setTo(255, mask);
			cv::subtract(groundTruthImg, maxDisp, diff, cv::noArray(), CV_32F);
			mask = diff > 0;
			red.setTo(255, mask);

			//set all masked pixels to blue
			blue = cv::Mat::zeros(green.size(), green.type());
			blue.setTo(255, maskImages.at(i) == 0);

			green.setTo(0, blue);
			green.setTo(0, red);
			red.setTo(0, blue);

			//set all regions that have not yet a disparity range estimation to white
			mask = minDisp > groundTruthImg.cols;
			red.setTo(255, mask);
			green.setTo(255, mask);
			blue.setTo(255, mask);

			cv::Mat channels[] = { blue, green, red };
			cv::merge(channels, 3, pngImg);

			if (params.maskFileNames.size() == 0) {
	//			cv::imshow(str.str(), pngImg);
				std::stringstream str;
				str << "debug-searchrange-okay-" << params.counter << ".png";
				cv::imwrite(str.str(), pngImg);
			} else {
				std::string filename = params.maskFileNames.at(i);
				size_t found = filename.rfind('/');
				if (found < filename.size() - 1)
					filename = filename.substr(found + 1);
				std::stringstream str;
				str << "debug-searchrange-okay-" << params.counter << "-" << filename;
				cv::imwrite(str.str(), pngImg);
				if (params.debug) cv::imshow(str.str(), pngImg);
			}
		}
		if (params.debug) cv::waitKey();
		if (params.debug) cv::destroyAllWindows();
	}

	if (params.verbosity & writeDebugConsoleOutput) {

		cv::Mat nrOfValidDisparities;
		cv::subtract(range.getMaxDisp(), range.getMinDisp(), nrOfValidDisparities, cv::noArray(), CV_16U);
		double dispValues = cv::norm(nrOfValidDisparities, cv::NORM_L1);
		double nrOfValidPixels = cv::countNonZero(nrOfValidDisparities);
		double maxDispValues = nrOfValidPixels * (params.maxDisparity - params.minDisparity);

		std::cout << "Percentage of pixels whose disparities will have been computed after this round: ";
		std::cout << nrOfValidPixels / range.getMinDisp().total() *100 << " %" << std::endl;
		std::cout << "Percentage of disparity values to compute"
				<< " in comparison to a fixed disparity search range"
				<< " from " << params.minDisparity << " to " << params.maxDisparity << ": "
				<< dispValues / maxDispValues * 100 << " %";
		std::cout << " (dispValues: " << (int) dispValues << ", maxDispValues: "
				<< (int) maxDispValues << ")" << std::endl;
	}
}

cv::Mat_<float> Evaluation::getGroundTruthImg() const {
	return groundTruthImg;
}

cv::Mat_<uint8_t> Evaluation::getMaskForValidPixels() const {
	int max = 0;
	int winner = 0;
	for (int i = 0; i < (int)maskImages.size(); ++i) {
		int count = cv::countNonZero(maskImages.at(i));
		if (count > max) {
			max = count;
			winner = i;
		}
	}
	return maskImages.at(winner);
}

void Evaluation::writeDiffToGroundTruthImg(
		const cv::Mat_<float>& disparityImage,
		const cv::Mat_<uint8_t>& mask,
		const std::string& filename = "diffToGroundTruth.png") const {
	cv::Mat red, green, blue;

	cv::Mat_<float> diff = cv::abs(disparityImage - groundTruthImg);
	cv::Mat diffWithThreshold;
	cv::threshold(diff, diffWithThreshold, params.errorThreshold, 0.0,
			cv::THRESH_TOZERO);

	cv::Mat maskedDiff;
	cv::normalize(diffWithThreshold, maskedDiff, 0, 255, cv::NORM_MINMAX,
			CV_8U);
	maskedDiff.setTo(0, mask == 0);
	if (params.verbosity & writeDebugImages) {
		cv::imwrite("debug-maskedDiff.png", maskedDiff);
	}

	if (flagMiddleburyStyle) {
		groundTruthImg.convertTo(green, CV_8U, scale);
	} else {
		green = (groundTruthImg - offset) * scale;
		green.convertTo(green, CV_8U, 255);
	}

	green.setTo(0, mask == 0);

	blue = green.clone();

	red = green + maskedDiff;

	cv::Mat tmpMask = red == 255;
	green.setTo(255, tmpMask);
	cv::subtract(green, maskedDiff, green, tmpMask);
	blue.setTo(255, tmpMask);
	cv::subtract(blue, maskedDiff, blue, tmpMask);

	tmpMask = maskedDiff == 0;
	blue.setTo(0, tmpMask);
	red.setTo(0, tmpMask);
	blue.setTo(255, mask == 0);

	cv::Mat pngImg;
	cv::Mat channels[] = { blue, green, red };
	cv::merge(channels, 3, pngImg);
	cv::imwrite(filename, pngImg);
}

void Evaluation::writeMeanDeviation(
		const cv::Mat_<float>& disparityImg,
		const cv::Mat_<uint8_t>& mask,
		const std::string& symbol) {
	cv::Mat_<float> diff = cv::abs(disparityImg - groundTruthImg);
	diff.setTo(0, mask == 0);

	cv::Mat onlyErrors;
	cv::threshold(diff, onlyErrors, params.errorThreshold, 255,
			CV_THRESH_BINARY);
	onlyErrors.convertTo(onlyErrors, CV_8U);

	cv::Scalar mean = cv::mean(diff, mask);
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Mean error of the computed disparity values (correct matches included): "
				<< mean[0] << std::endl;
	if (logFile.good())
		logFile << symbol << " " << mean[0] << std::endl;

	mean = cv::mean(diff, onlyErrors);
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Mean error of the wrongly computed disparity values (only wrong matches whose difference exceeds the threshold): "
				<< mean[0] << std::endl;
}

void Evaluation::writePercentageOfBadPixels(
		const cv::Mat_<float>& disparityImage,
		const cv::Mat_<uint8_t>& mask,
		const std::string& symbol,
		const std::string& description) {
	mask &= maskImages.at(0);

	cv::Mat_<float> diff = cv::abs(disparityImage - groundTruthImg);
	//eliminate errors that are not larger than the threshold (default: 1)
	cv::Mat diffWithThreshold;
	cv::threshold(diff, diffWithThreshold, params.errorThreshold, 0.0,
			cv::THRESH_TOZERO);

	//use the mask
	diffWithThreshold.setTo(0, mask == 0);
	double wrongDisparities = cv::countNonZero(diffWithThreshold);
	double total = cv::countNonZero(mask);
	double wrongPercentage = wrongDisparities / total;

	writeAsPercentage(wrongPercentage, symbol, description);
}

void Evaluation::writeAsPercentage(double percentage, const std::string& symbol,
		const std::string& description) {
	if (logFile.good())
		logFile << symbol << " " << percentage * 100 << std::endl;

	if (params.verbosity & writeBasicConsoleOutput) {
		std::cout << description << ": " << percentage * 100 << " %"
				<< std::endl;
	}
}

void Evaluation::readGroundTruthImg() {
	cv::Mat origImg = cv::imread(params.groundTruthFileName,
			cv::IMREAD_UNCHANGED);

	// some images are loaded as 3-channel images, we need 1 channel
	if (origImg.channels() > 1) {
		std::vector<cv::Mat> channels;
		cv::split(origImg, channels);
		origImg = channels[0];
	}
	origImg.convertTo(groundTruthImg, CV_32F);

	extractOffsetAndScaleFromFilename(params.groundTruthFileName);
	groundTruthImg.convertTo(groundTruthImg, CV_32F, 1 / scale, offset);

	if (params.verbosity & writeDebugConsoleOutput) {
		std::cout << "compare to ground truth: " << params.groundTruthFileName;
		std::cout << " (offset: " << offset << " scale: " << scale << ")";
		std::cout << std::endl;
	}
}

void Evaluation::extractOffsetAndScaleFromFilename(const std::string& fileName) {
	std::istringstream stream;
	size_t pos = fileName.find("offset_");
	if (pos < fileName.size()) {
		stream.str(fileName.substr(pos + 7));
		stream >> offset;
	} else {
		offset = 0.0;
		flagMiddleburyStyle = true;
	}
	pos = fileName.find("scale_");
	if (pos < fileName.size()) {
		stream.str(fileName.substr(pos + 6));
		stream >> scale;
	} else {
		scale = 1.0;
	}
}

void Evaluation::readMasks() {
	if (params.maskFileNames.size() == 0) {
		cv::Mat_<uint8_t> maskImg = cv::Mat(groundTruthImg.size(), CV_8U, 255);
		maskImages.push_back(maskImg);
	} else {
		for (size_t i = 0; i < params.maskFileNames.size(); ++i) {
			std::string filename = params.maskFileNames.at(i);
			cv::Mat origImg = cv::imread(filename, cv::IMREAD_UNCHANGED);
			if (origImg.channels() > 1) {
				std::vector<cv::Mat> channels;
				cv::split(origImg, channels);
				origImg = channels[0];
			}
			cv::Mat_<uint8_t> maskImg = cv::Mat_<uint8_t>(origImg.size());
			cv::compare(origImg, 255, maskImg, cv::CMP_EQ); // eliminate grey values in Middlebury masks
			if (cv::countNonZero(maskImg) == 0) // is a Häming-style occlusion map in exr format
				cv::compare(origImg, 1.0 - 2 * FLT_EPSILON, maskImg, cv::CMP_LE);
			maskImages.push_back(maskImg);
			if (params.verbosity & writeDebugImages) {
				if (filename.substr(filename.size() - 4, 4).find(".png") == 4)
					filename += ".png";
				cv::imwrite("mask-" + filename, maskImg);
			}
		}
	}
}

void Evaluation::writeMiddleburyDispImg(cv::Mat_<float> floatWTAImg) {
	if (scale < 0) {
		cv::Mat_<uint8_t> pngImg;
		floatWTAImg.convertTo(pngImg, CV_8U, scale);
		cv::imwrite("disparityImage-middlebury.png", pngImg);
	}
}

void Evaluation::writeImg(const std::string& filename, const cv::Mat& img) const {
	assert(img.channels() == 1);

	std::cout << "Writing image: " << filename << " ... " << std::flush;
	cv::Mat pngImg, tmp;

	double min, max;
	cv::Point minPoint, maxPoint;
	tmp = img.clone();
	cv::minMaxLoc(tmp, &min, &max, &minPoint, &maxPoint, (img < img.cols) & (img > -img.cols));
	tmp.setTo(min, img > img.cols);
	tmp.setTo(max, img < -img.cols);

	cv::normalize(tmp, pngImg, 0, 255, cv::NORM_MINMAX, CV_8U);
	cv::Mat channels[] = { pngImg, pngImg, pngImg };
	cv::merge(channels, 3, pngImg);

	cv::circle(pngImg, minPoint, 3, cv::Scalar(0, 255, 0), 1);
	std::stringstream text;
	text << min << " at " << minPoint;
	int fontFace = cv::FONT_HERSHEY_PLAIN;
	double fontScale = 1;
	int thickness = 1;
	int baseline = 0;
	cv::Size size = cv::getTextSize(text.str(), fontFace, fontScale, thickness, &baseline);
	if (minPoint.x + size.width >= img.cols) minPoint.x = img.cols - size.width;
	if (minPoint.y < size.height) minPoint.y = size.height;
	cv::putText(pngImg, text.str(), minPoint, fontFace, fontScale, cv::Scalar(0,0,255), thickness);

	cv::circle(pngImg, maxPoint, 3, cv::Scalar(0, 255, 0), 1);
	text.str("");
	text << max << " at " << maxPoint;
	size = cv::getTextSize(text.str(), fontFace, fontScale, thickness, &baseline);
	if (maxPoint.x + size.width >= img.cols) maxPoint.x = img.cols - size.width;
	if (maxPoint.y < size.height) maxPoint.y = size.height;
	cv::putText(pngImg, text.str(), maxPoint, fontFace, fontScale, cv::Scalar(0,0,255), thickness);

	cv::imwrite(filename, pngImg);
	if (params.debug) cv::imshow(filename, pngImg);

	std::cout << "done." << std::endl;
}
