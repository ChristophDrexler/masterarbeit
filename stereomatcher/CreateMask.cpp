/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <cfloat>
#include <iostream>
#include <opencv2/opencv.hpp>

void readme() {
	std::cout << " Usage:" << std::endl;
	std::cout << "./createMask <rightRectDispOcc_*.exr> to create the visibility masks all.png and nonocc.png" << std::endl;
	std::cout << "./createMask <rect_rechts_licht1.png> to create the valid pixel mask valid.png" << std::endl;
}

int main(int argc, char** argv) {
	if (argc != 2) {
		readme();
		return -1;
	}

	cv::Mat src = cv::imread(argv[1], cv::IMREAD_UNCHANGED);
	cv::Mat dest;
	if (src.depth() == CV_32F) {
		if (src.channels() > 1) {
			std::vector<cv::Mat> channels;
			cv::split(src, channels);
			src = channels[0];
		}
		cv::compare(src, 1.0 - 2 * FLT_EPSILON, dest, cv::CMP_LE);
		cv::imwrite("nonocc.png", dest);
		cv::compare(src, 1.0 - FLT_EPSILON, dest, cv::CMP_LE);
		cv::imwrite("all.png", dest);
	} else {
		src.convertTo(src, CV_16U);
		std::vector<cv::Mat> channels;
		cv::split(src, channels);
		src = channels[0] + channels[1] + channels[2];

		cv::compare(src, cv::Scalar(0), dest, cv::CMP_NE);
		cv::imwrite("valid.png", dest);
	}
}
