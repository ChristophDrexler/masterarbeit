/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef DISPARITYSPACEIMAGE_H_
#define DISPARITYSPACEIMAGE_H_

#include <iostream>
#include <limits>
#include <stdint.h>
#include <vector>
#include <opencv2/opencv.hpp>

#include "DisparitySearchRange.h"
#include "DSIPixel.h"
#include "Image.h"

class DisparitySpaceImage {
private:
	const Parameters& params;
	const Image& baseImg;
	int16_t minDisparity, maxDisparity;
	cv::Mat_<uint8_t> validPixels, alreadyComputedPixels;
	std::vector<DSIPixel> dsi;
	cv::Mat_<int16_t> intWTAImage;
	cv::Mat_<float> floatWTAImage;
	cv::Mat_<int16_t> matchIntWTAImage;
	bool isValidWTAImage;
	bool isValidMatchWTAImage;
public:
	DisparitySpaceImage(const Parameters& p, const Image& baseImg);
	DisparitySpaceImage(const Parameters& p,
			const Image& baseImg,
			const int16_t minDisparity, const int16_t maxDisparity,
			uint16_t initialValue);
	void createDataStructures(const int16_t minDisparity,
			const int16_t maxDisparity, datatype initialValue);
	void createDataStructures(DisparitySearchRange& range, datatype initialValue);

	datatype valueAt(int y, int x, int disp) const;
	typename std::vector<datatype>::reference at(int y, int x, int disp);
	const DSIPixel& at(int y, int x) const;
	DSIPixel& at(int y, int x);
	bool isValid(int y, int x, int disp) const { return at(y,x).isDisparityValid(disp); }

	int getCols() const { return baseImg.getCols(); };
	int getRows() const { return baseImg.getRows(); };
	int16_t getMinDisparity() const { return minDisparity; }
	int16_t getMaxDisparity() const { return maxDisparity; }
	int getDisparityRange() const { return maxDisparity - minDisparity; }
	datatype getMaxCost() const { return std::numeric_limits<datatype>::max(); }
	void getMaxValidCost(cv::Point& point, datatype& cost) const;
	int16_t getInvalidDisparity() const { return minDisparity - 1; }
	cv::Mat_<uint8_t>& getComputedPixelsMat() { return alreadyComputedPixels; }
	const cv::Mat_<uint8_t>& getComputedPixelsMat() const { return alreadyComputedPixels; }
	cv::Mat_<uint8_t> getComputedValidPixelsMat() const { return alreadyComputedPixels & validPixels; }
	std::vector<DSIPixel>& getData() { return dsi; }

	void setValidPixelsToComputedPixels();
	void addWeighted(DisparitySpaceImage& addedDSI, uint8_t scale = 1);

	void computeWTAImage();
	void computeMatchIntWTAImg(const Image& matchImg);
	void blurWTAImage();
	const cv::Mat_<int16_t>& getIntWTAImage();
	const cv::Mat_<int16_t>& getMatchIntWTAImage(const Image& matchImg);
	const cv::Mat_<float>& getFloatWTAImage();
	void writeWTAImage(const std::string filename, cv::Mat_<uint8_t> mask = cv::Mat_<uint8_t>());
	void writeWTAImageOfMatchImg(const Image& matchImg,
			const std::string filename);
	void getCostAt(const cv::Mat_<int16_t>& dispImg,
			cv::Mat_<datatype>& costImg,
			cv::Mat_<uint8_t>& validDisparitiesMask) const;
	void writeCostAt(const cv::Mat& dispImg,
			const std::string filename,
			const cv::Mat_<uint8_t> mask = cv::Mat_<uint8_t>()) const;
	void writeCostAtWTAImg(const std::string filename,
			const cv::Mat_<uint8_t> mask = cv::Mat_<uint8_t>());
	void writeDSI(int fromX = 0, int toX = -1, int fromY = 0, int toY = -1) const;
};

#endif /* DISPARITYSPACEIMAGE_H_ */
