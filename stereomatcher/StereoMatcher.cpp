/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <iostream>

#include "CensusFilter.h"
#include "Confidence.h"
#include "DisparitySearchRange.h"
#include "Error.h"
#include "SGMAggregation.h"
#include "StereoMatcher.h"
#include "Timer.h"

void StereoMatcher::start() {
	if (params.baseFeaturesFileName != "" && params.matchFeaturesFileName != "") {
		startWithSearchRangeEstimatedByFeatures();
	} else {
		if (params.verbosity & writeDebugConsoleOutput)
			std::cout << "disparity range: from " << params.minDisparity
			<< " to " << params.maxDisparity << std::endl;
		startWithFixedSearchRange();
	}
}

void StereoMatcher::startWithFixedSearchRange() {
	assert(params.minDisparity < 10000 && params.maxDisparity > -10000);

	Evaluation evaluation(params);

	writeBaseAndMatchImg();

	costDSI.createDataStructures(params.minDisparity, params.maxDisparity, costDSI.getMaxCost());
	std::auto_ptr<CostFunction> costFunction = getCostFunction();
	costFunction->compute();

	if (params.verbosity & (writeDebugImages | writeImportantImages)) {
		costDSI.writeCostAt(evaluation.getGroundTruthImg(),
				"debug-costAtGroundTruth-costDSI.png",
				evaluation.getMaskForValidPixels());
	}

	std::auto_ptr<Aggregation> aggregationMethod = getAggregationMethod();
	aggregatedCostDSI.createDataStructures(params.minDisparity, params.maxDisparity, 0);
	aggregationMethod->aggregate();

	evaluation.evaluate(aggregatedCostDSI);

	cv::Mat_<uint8_t> validPixels = getValidPixelsMask();
	writeDisparityImages(validPixels);
	evaluation.writeMiddleburyDispImg(aggregatedCostDSI.getFloatWTAImage());
	evaluateResult(evaluation, aggregatedCostDSI.getFloatWTAImage(), validPixels);
}

void StereoMatcher::startWithSearchRangeEstimatedByFeatures() {
//	params.debug = true;
	std::auto_ptr<CostFunction> costFunction = getCostFunction();
	std::auto_ptr<Aggregation> aggregationMethod = getAggregationMethod();

	writeBaseAndMatchImg();

	SupportRegion support(params, baseImg);
	DisparitySearchRange range(params, baseImg, support);
	range.calculateByInterpolatingFeatureDisparitiesAndPropagation();

	cv::Mat_<int16_t> minDisp = range.getMinDisp();
	cv::Mat_<int16_t> maxDisp = range.getMaxDisp();

	Evaluation evaluation(params);
	cv::Mat_<uint8_t>& computedPixels = aggregatedCostDSI.getComputedPixelsMat();

	while (cv::countNonZero(computedPixels) < (int)computedPixels.total() * params.percentageToCompute
			&& params.counter < params.maxRounds) {
		if (params.verbosity & (writeBasicConsoleOutput | writeDebugConsoleOutput))
			std::cout << " ******* Round: " << params.counter << std::endl;

		if (params.counter > 0) {
			range.setTo(aggregatedCostDSI.getIntWTAImage(), aggregatedCostDSI.getComputedValidPixelsMat());
			invalidateInsecurePixels();
		}

		range.findMissingDispSearchRanges();

		evaluation.evaluate(range);

		costDSI.createDataStructures(range, costDSI.getMaxCost());
		aggregatedCostDSI.createDataStructures(range, 0);

		costFunction->compute();

		if (params.verbosity & (writeDebugImages | writeImportantImages)) {
			std::stringstream str;
			str << "debug-costAtGroundTruth-costDSI-" << params.counter << ".png";
			costDSI.writeCostAt(evaluation.getGroundTruthImg(),
					str.str());
		}

		aggregationMethod->aggregate();

		evaluation.evaluate(aggregatedCostDSI);

		++params.counter;
//		if (params.counter > 0) params.debug = true;
	}

//	aggregatedCostDSI.blurWTAImage();
	cv::Mat_<uint8_t> validPixels = getValidPixelsMask();
	writeDisparityImages(validPixels);
	evaluateResult(evaluation, aggregatedCostDSI.getFloatWTAImage(), validPixels);
}

std::auto_ptr<CostFunction> StereoMatcher::getCostFunction() {
	switch (params.costFunction) {
	case functionCensus:
		params.setThreshold(30);
		params.setCensusParameters(9, 7, 1);
		return std::auto_ptr<CostFunction>(new CensusFilter(params, baseImg, matchImg, costDSI));
		break;
	default:
		throw Error("Unknown cost function");
		break;
	}
	return std::auto_ptr<CostFunction>(new CensusFilter(params, baseImg, matchImg, costDSI));
}

std::auto_ptr<Aggregation> StereoMatcher::getAggregationMethod() {
	switch (params.aggregationMethod) {
	case aggregationSGM:
		params.setSGMParameters(40, 5000); // set default parameters if no custom parameters have been set
		return std::auto_ptr<Aggregation>(new SGMAggregation(params, costDSI, aggregatedCostDSI, baseImg));
		break;
	default:
		throw Error("Unknown aggregation method");
		break;
	}
	params.setSGMParameters(40, 5000); // set default parameters if no custom parameters have been set
	return std::auto_ptr<Aggregation>(new SGMAggregation(params, costDSI, aggregatedCostDSI, baseImg));
}

void StereoMatcher::calculateConfidenceMasks(const cv::Mat_<int16_t> intDispImg,
		const cv::Mat_<float> floatDispImg, Evaluation& eval) {
	Confidence conf(params, aggregatedCostDSI);
	cv::Mat valuesPKRN = conf.getValuesPKRN();
	cv::Mat valuesMSM = conf.getValuesMSM(intDispImg);
	cv::Mat valuesLRC = conf.getValuesLRC(aggregatedCostDSI.getFloatWTAImage(),
			aggregatedCostDSI.getMatchIntWTAImage(matchImg));
	if (params.verbosity & writeDebugImages) {
		std::stringstream str;
		str << "debug-confidence-PKRN-" << params.counter << ".png";
		writeImg(str.str(), valuesPKRN);
		str.str("");
		str << "debug-confidence-MSM-" << params.counter << ".png";
		writeImg(str.str(), valuesMSM);
		str.str("");
		str << "debug-confidence-LRC-" << params.counter << ".png";
		writeImg(str.str(), valuesLRC);
	}

	Timer timer;
	timer.start();
	cv::Mat maskPKRN = conf.getMask(valuesPKRN, conf.getThreshold(valuesPKRN, 79, 81));
	cv::Mat maskMSM = conf.getMask(valuesMSM, conf.getThreshold(valuesMSM, 79, 81));
	cv::Mat maskLRC = conf.getMask(valuesLRC, -1.0);

	if (params.groundTruthFileName.size() > 0) {
		eval.writePercentageOfBadPixels(floatDispImg, maskPKRN,
				"wrongPercentageTrustedOnlyPKRN",
				"Percentage of wrongly computed disparities which nevertheless have a high trusted PKRN value");
		eval.writeDiffToGroundTruthImg(floatDispImg, maskPKRN,
				"diffToGroundTruth-trusted-PKRN.png");

		eval.writePercentageOfBadPixels(floatDispImg, maskMSM,
				"wrongPercentageTrustedOnlyMSM",
				"Percentage of wrongly computed disparities which nevertheless have a high trusted MSM value");
		eval.writeDiffToGroundTruthImg(floatDispImg, maskMSM,
				"diffToGroundTruth-trusted-MSM.png");

		eval.writePercentageOfBadPixels(floatDispImg, maskLRC,
				"wrongPercentageTrustedOnlyLRC",
				"Percentage of wrongly computed disparities which nevertheless have a high trusted LRC value");
		if (params.verbosity & writeDebugConsoleOutput) {
			std::cout << "Percentage of values that pass the LRC-Check: ";
			std::cout << (double) (cv::countNonZero(maskLRC)) / maskLRC.total() * 100 << " %" << std::endl;
		}
		eval.writeDiffToGroundTruthImg(floatDispImg, maskLRC,
				"diffToGroundTruth-trusted-LRC.png");
	}
}

void StereoMatcher::invalidateInsecurePixels() {
	cv::Mat_<uint8_t> invalidatedPixelsMask(baseImg.size());
	invalidatedPixelsMask = 255;

	markInsecurePixels(invalidatedPixelsMask);
	markPixelsAroundInsecurePixels(invalidatedPixelsMask, 10);

	cv::Mat_<uint8_t>& computedCostPixels = costDSI.getComputedPixelsMat();
	cv::Mat_<uint8_t>& aggregatedPixels = aggregatedCostDSI.getComputedPixelsMat();
	computedCostPixels.setTo(0, invalidatedPixelsMask);
	aggregatedPixels.setTo(0, invalidatedPixelsMask);

	if (params.verbosity & writeDebugImages) {
		std::stringstream str;
		str << "debug-invalidatedPixels-" << params.counter << ".png";
		writeImg(str.str(), invalidatedPixelsMask);
	}
}

void StereoMatcher::markInsecurePixels(cv::Mat_<uint8_t>& invalidatedPixelsMask) {
	cv::Mat_<uint8_t>& aggregatedPixels =
			aggregatedCostDSI.getComputedPixelsMat();
	cv::Mat_<int16_t> wtaImg = aggregatedCostDSI.getIntWTAImage();

	for (int y = 0; y < aggregatedCostDSI.getRows(); ++y) {
		for (int x = 0; x < aggregatedCostDSI.getCols(); ++x) {
			if (aggregatedPixels(y, x)) {
				DSIPixel& pixel = aggregatedCostDSI.at(y, x);
				if (wtaImg(y, x) == pixel.getMinDisparity()
						|| wtaImg(y, x) == pixel.getMaxDisparity()) {
					invalidatedPixelsMask(y, x) = 0;
				}
			}
		}
	}
}

void StereoMatcher::markPixelsAroundInsecurePixels(cv::Mat_<uint8_t>& invalidatedPixelsMask, int radius) {
	assert(radius >= 0);

	cv::Mat distanceMat;
	cv::distanceTransform(invalidatedPixelsMask, distanceMat, CV_DIST_L2, CV_DIST_MASK_3);
	cv::compare(distanceMat, radius, invalidatedPixelsMask, cv::CMP_LE);

}

void StereoMatcher::writeBaseAndMatchImg() {
	if (params.verbosity & writeDebugImages) {
		if (params.verbosity & writeDebugConsoleOutput)
			std::cout << "Writing image: debug-orig-baseImg.png" << std::endl;

		cv::imwrite("debug-orig-baseImg.png", baseImg.getMat());
		if (params.verbosity & writeDebugConsoleOutput)
			std::cout << "Writing image: debug-orig-matchImg.png" << std::endl;

		cv::imwrite("debug-orig-matchImg.png", matchImg.getMat());
	}
}

cv::Mat_<uint8_t> StereoMatcher::getValidPixelsMask() {
	Confidence conf(params, aggregatedCostDSI);
	cv::Mat_<float> valuesLRC = conf.getValuesLRC(
			aggregatedCostDSI.getFloatWTAImage(),
			aggregatedCostDSI.getMatchIntWTAImage(matchImg));
	cv::Mat_<float> valuesMSM = conf.getValuesMSM(
			aggregatedCostDSI.getIntWTAImage());
	cv::Mat_<uint8_t> maskLRC = conf.getMask(valuesLRC, -1.0);
	cv::Mat_<uint8_t> maskMSM = conf.getMask(valuesMSM,
			-params.sgm.penalty1 * 10);
	cv::Mat_<uint8_t> mask = maskLRC & maskMSM;
	return mask;
}

void StereoMatcher::writeDisparityImages(cv::Mat_<uint8_t> validPixelsMask) {
	if (params.verbosity & (writeOnlyDisparityImage | writeImportantImages | writeDebugImages)) {
		aggregatedCostDSI.writeWTAImage("disparityImg-allcomputedpixels.png");
		aggregatedCostDSI.writeWTAImage("disparityImg-trustedpixels.png", validPixelsMask);
	}
	if (params.verbosity & writeDebugImages)
		aggregatedCostDSI.writeWTAImageOfMatchImg(matchImg, "debug-matchDisparityImg.png");
}

void StereoMatcher::evaluateResult(Evaluation& evaluation,
		cv::Mat_<float> disparityImg, cv::Mat_<uint8_t> validPixelsMask) {
	if (params.verbosity & writeDebugImages)
		calculateConfidenceMasks(aggregatedCostDSI.getIntWTAImage(),
				aggregatedCostDSI.getFloatWTAImage(), evaluation);

	if (params.verbosity & (writeDebugImages | writeImportantImages)) {
		aggregatedCostDSI.writeCostAtWTAImg("costAtWTAImg.png");
		aggregatedCostDSI.writeCostAt(evaluation.getGroundTruthImg(),
				"costAtGroundTruth.png");
		evaluation.writeDiffToGroundTruthImg(disparityImg, validPixelsMask,
				"diffToGroundTruth-trusted.png");
	}

	if (params.verbosity & (writeBasicConsoleOutput | writeDebugConsoleOutput)) {
		double computedPixels = (float) cv::countNonZero(aggregatedCostDSI.getComputedValidPixelsMat())
					/ (float) cv::countNonZero(baseImg.getValidPixelsMask());
		evaluation.writeAsPercentage(computedPixels, "PercentageComputedPixels",
				"Percentage of computed pixels");

		double trustedPixels = (float) cv::countNonZero(validPixelsMask)
		/ (float) cv::countNonZero(baseImg.getValidPixelsMask());
		evaluation.writeAsPercentage(trustedPixels, "PercentageTrustedPixels",
				"Percentage of trusted pixels");

		evaluation.writePercentageOfBadPixels(disparityImg, validPixelsMask,
				"PercentageWrongTrustedPixels",
				"Percentage of wrongly calculated disparites among the trusted disparity values");
	}
}

void StereoMatcher::writeImg(const std::string& filename,
		const cv::Mat& img) const {
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Writing image: " << filename << " ..." << std::flush;
	cv::Mat pngImg;
	cv::normalize(img, pngImg, 0, 255, cv::NORM_MINMAX, CV_8U);
	cv::imwrite(filename, pngImg);
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << " done." << std::endl;
}

void StereoMatcher::writeHistogram(const std::string& filename,
		const cv::Mat_<float>& img) const {
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Writing image: " << filename << " ..." << std::flush;

	double min, max; cv::Point minPoint, maxPoint;
	cv::minMaxLoc(img, &min, &max, &minPoint, &maxPoint);
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << " (min: " << min << ", max: " << max << ", maxPoint: " << maxPoint << std::flush;

	int histSize = 1000;
	float range[] = { (float) min, (float) max };
	const float* histRange = { range };
	cv::Mat histogram;
	bool uniform=true, accumulate=false;
	cv::calcHist(&img, 1, 0, cv::noArray(), histogram, 1, &histSize, &histRange, uniform, accumulate );

	cv::minMaxLoc(histogram, &min, &max, &minPoint, &maxPoint);
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << ", most frequent value: " << maxPoint.y << ")" << std::flush;
	int histHeight = 900;
	cv::Mat histImg(histHeight, histSize, CV_8UC1, cv::Scalar(0));
	cv::normalize(histogram, histogram, 1, histImg.rows, cv::NORM_MINMAX);
	for( int i = 0; i <= histSize; i++ ) {
	    cv::line( histImg, cv::Point(i, histHeight - 1) ,
	                     cv::Point(i, histHeight - cvRound(histogram.at<float>(i)) ),
	                     cv::Scalar(255), 1, 8, 0  );
	}
	cv::Mat zeros(histImg.size(), CV_8UC1, cv::Scalar(0));
	cv::Mat channels[] = { zeros, histImg, zeros };
	cv::merge(channels, 3, histImg);
	cv::imwrite(filename, histImg );
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << " done." << std::endl;
}
