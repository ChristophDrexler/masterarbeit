/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <cassert>
#include <iostream>
#include <limits>
#include <stdint.h>

#include "DisparitySpaceImage.h"
#include "DSIPixel.h"
#include "Timer.h"

DisparitySpaceImage::DisparitySpaceImage(const Parameters& p, const Image& baseImg) :
				params(p),
				baseImg(baseImg),
				minDisparity(std::numeric_limits<int16_t>::max()),
				maxDisparity(std::numeric_limits<int16_t>::min()),
				validPixels(cv::Mat_<uint8_t>(baseImg.size())),
				alreadyComputedPixels(cv::Mat_<uint8_t>(baseImg.size())),
				isValidWTAImage(false),
				isValidMatchWTAImage(false) {
	alreadyComputedPixels = ~baseImg.getValidPixelsMask();
	validPixels = 0;
}

DisparitySpaceImage::DisparitySpaceImage(const Parameters& p,
		const Image& baseImg,
		const int16_t minDisparity, const int16_t maxDisparity,
		uint16_t initialValue) :
				params(p),
				baseImg(baseImg),
				minDisparity(minDisparity), maxDisparity(maxDisparity),
				validPixels(cv::Mat_<uint8_t>(baseImg.size())),
				alreadyComputedPixels(cv::Mat_<uint8_t>(baseImg.size())),
				isValidWTAImage(false),
				isValidMatchWTAImage(false) {
	assert(minDisparity <= maxDisparity);

	alreadyComputedPixels = ~baseImg.getValidPixelsMask();
	validPixels = 0;

	createDataStructures(minDisparity, maxDisparity, initialValue);
}

void DisparitySpaceImage::createDataStructures(const int16_t minDisparity,
		const int16_t maxDisparity, uint16_t initialValue) {
	assert(minDisparity <= maxDisparity);

	this->minDisparity = minDisparity;
	this->maxDisparity = maxDisparity;
	dsi = std::vector<DSIPixel>(baseImg.getRows() * baseImg.getCols(),
			DSIPixel(minDisparity, maxDisparity, initialValue));
	isValidWTAImage = isValidMatchWTAImage = false;
	validPixels = baseImg.getValidPixelsMask();
}

void DisparitySpaceImage::createDataStructures(DisparitySearchRange& range, uint16_t initialValue) {
	if (dsi.size() == 0)
		dsi = std::vector<DSIPixel>(baseImg.getRows() * baseImg.getCols());

	const cv::Mat_<int16_t>& minDispMat = range.getMinDisp();
	const cv::Mat_<int16_t>& maxDispMat = range.getMaxDisp();
	for (int y = 0; y < getRows(); ++y) {
		for (int x = 0; x < getCols(); ++x) {
			if (alreadyComputedPixels(y, x) == 0
					&& minDispMat(y, x) < minDispMat.cols) {
				DSIPixel& pixel = at(y,x);
				DSIPixel oldPixel = pixel;
				int16_t minDisp = minDispMat(y, x);
				int16_t maxDisp = maxDispMat(y, x);
				pixel.resize(minDisp, maxDisp, initialValue);
				for (int disp = oldPixel.getMinDisparity(); disp < oldPixel.getMaxDisparity(); ++disp) {
					if (disp >= minDisp && disp < maxDisp)
						pixel.at(disp) = oldPixel.at(disp);
				}
				validPixels(y,x) = 255;

				if (minDisp < minDisparity)
					minDisparity = minDisp;
				if (maxDisp > maxDisparity)
					maxDisparity = maxDisp;
			}
		}
	}
	isValidWTAImage = isValidMatchWTAImage = false;
}

uint16_t DisparitySpaceImage::valueAt(int y, int x, int disp) const {
	assert(x >= 0);
	assert(x < getCols());
	assert(y >= 0);
	assert(y < getRows());

	return dsi[y * getCols() + x].valueAt(disp);
}

std::vector<uint16_t>::reference DisparitySpaceImage::at(int y, int x,
		int disp) {
	assert(x >= 0);
	assert(x < getCols());
	assert(y >= 0);
	assert(y < getRows());

	return dsi[y * getCols() + x].at(disp);
}

const DSIPixel& DisparitySpaceImage::at(int y, int x) const {
	assert(x >= 0);
	assert(x < getCols());
	assert(y >= 0);
	assert(y < getRows());

	return dsi[y * getCols() + x];
}

DSIPixel& DisparitySpaceImage::at(int y, int x) {
	assert(x >= 0);
	assert(x < getCols());
	assert(y >= 0);
	assert(y < getRows());

	return dsi[y * getCols() + x];
}

void DisparitySpaceImage::getMaxValidCost(cv::Point& point, datatype& cost) const {
	point = cv::Point(-1, -1);
	cost = 0;
	for (int y = 0; y < getRows(); ++y) {
		for (int x = 0; x < getCols(); ++x) {
			for (int disp = minDisparity; disp < maxDisparity; ++disp) {
				if (valueAt(y, x, disp) < getMaxCost()) {
					if (valueAt(y, x, disp) > cost) {
						point.x = x;
						point.y = y;
						cost = valueAt(y, x, disp);
					}
				}
			}
		}
	}
}

void DisparitySpaceImage::addWeighted(DisparitySpaceImage& addedDSI,
		uint8_t scale) {
	for (int y = 0; y < getRows(); ++y) {
		for (int x = 0; x < getCols(); ++x) {
			DSIPixel& pixel1 = at(y, x);
			DSIPixel& pixel2 = addedDSI.at(y, x);
			for (int disp = pixel1.getMinDisparity();
					disp < pixel1.getMaxDisparity(); ++disp) {
				pixel1.at(disp) = cv::saturate_cast<uint16_t>(
						pixel1.valueAt(disp) + pixel2.valueAt(disp) * scale);
			}
		}
	}
}

void DisparitySpaceImage::setValidPixelsToComputedPixels() {
	for (int y = 0; y < getRows(); ++y) {
		for (int x = 0; x < getCols(); ++x) {
			if (at(y,x).isValid())
				alreadyComputedPixels(y,x) = 255;
		}
	}
	isValidWTAImage = isValidMatchWTAImage = false;
}

void DisparitySpaceImage::computeWTAImage() {
	Timer timer;
	timer.start();
	intWTAImage.create(baseImg.size());
	floatWTAImage.create(baseImg.size());
	uint16_t leastCost;
	int16_t intDisparity;
	float floatDisparity;

	for (int y = 0; y < getRows(); ++y) {
		for (int x = 0; x < getCols(); ++x) {
			const DSIPixel& pixel = at(y, x);
			if (pixel.isValid()) {
				pixel.getLeastCost(leastCost, intDisparity);
				if (leastCost == getMaxCost()) {
					intDisparity = getInvalidDisparity();
					floatDisparity = getInvalidDisparity();
				} else if (intDisparity > pixel.getMinDisparity()
						&& intDisparity < pixel.getMaxDisparity() - 1) {
					// compute subpixel value using a quadratic curve interpolation
					floatDisparity = intDisparity
						+ (pixel.valueAt(intDisparity - 1) - pixel.valueAt(intDisparity + 1))
						/ ( 2 * (pixel.valueAt(intDisparity - 1) - 2 * (float) leastCost + pixel.valueAt(intDisparity + 1)) );
				} else {
					floatDisparity = intDisparity;
				}
			} else {
				intDisparity = getInvalidDisparity();
				floatDisparity = getInvalidDisparity();
			}
			intWTAImage(y,x) = intDisparity;
			floatWTAImage(y,x) = floatDisparity;
		}
	}
	isValidWTAImage = true;
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for computing the base disparity images at integer and float precision: " << timer.stop() << " s" << std::endl;
}

void DisparitySpaceImage::computeMatchIntWTAImg(const Image& matchImg) {
	Timer timer;
	timer.start();

	matchIntWTAImage.create(matchImg.size());
	uint16_t leastCost;
	int16_t disparity;
	for (int y = 0; y < matchImg.getRows(); ++y) {
		for (int x = 0; x < matchImg.getCols(); ++x) {
			leastCost = getMaxCost();
			for (int disp = minDisparity; disp < maxDisparity; ++disp) {
				if (matchImg.isValid(y,x) && baseImg.isValid(y,x-disp) && valueAt(y, x-disp, disp) < leastCost) {
					leastCost = valueAt(y, x-disp, disp);
					disparity = -disp;
				}
			}
			if (leastCost == getMaxCost())
				disparity = -getInvalidDisparity();
			matchIntWTAImage(y,x) = disparity;
		}
	}
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for computing the reference disparity image at integer precision: " << timer.stop() << " s" << std::endl;
}

void DisparitySpaceImage::blurWTAImage() {
	//remove outliers
	cv::medianBlur(intWTAImage, intWTAImage, 3);
	cv::medianBlur(floatWTAImage, floatWTAImage, 3);
}

const cv::Mat_<int16_t>& DisparitySpaceImage::getIntWTAImage() {
	if (!isValidWTAImage) computeWTAImage();
	return intWTAImage;
}

const cv::Mat_<float>& DisparitySpaceImage::getFloatWTAImage() {
	if (!isValidWTAImage) computeWTAImage();
	return floatWTAImage;
}

const cv::Mat_<int16_t>& DisparitySpaceImage::getMatchIntWTAImage(const Image& matchImg) {
	if (!isValidMatchWTAImage) computeMatchIntWTAImg(matchImg);
	return matchIntWTAImage;
}

void DisparitySpaceImage::writeWTAImage(const std::string filename, cv::Mat_<uint8_t> mask) {
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Writing image: " << filename << " ... " << std::endl;

	if (mask.dims < 2) {
		mask.create(baseImg.size());
		mask = 255;
	}
	cv::Mat pngImg;
	cv::normalize(getFloatWTAImage(), pngImg, 0, 255, cv::NORM_MINMAX, CV_8U);
	cv::imwrite(filename, pngImg & mask);
}

void DisparitySpaceImage::writeWTAImageOfMatchImg(const Image& matchImg,
		const std::string filename) {
	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Writing image: " << filename << " ... " << std::flush;
	cv::Mat pngImg;
	cv::normalize(getMatchIntWTAImage(matchImg), pngImg, 0, 255, cv::NORM_MINMAX, CV_8U);
	cv::imwrite(filename, pngImg);
	std::cout << "done." << std::endl;
}

void DisparitySpaceImage::getCostAt(
		const cv::Mat_<int16_t>& dispImg,
		cv::Mat_<uint16_t>& costImg,
		cv::Mat_<uint8_t>& validDisparitiesMask) const {
	assert(dispImg.rows == getRows());
	assert(dispImg.cols == getCols());
	costImg.create(baseImg.size());
	validDisparitiesMask.create(baseImg.size());
	for (int y = 0; y < dispImg.rows; ++y) {
		for (int x = 0; x < dispImg.cols; ++x) {
			int16_t disp = dispImg(y, x);
			const DSIPixel& pixel = at(y, x);
			if (disp == getInvalidDisparity()
					|| !pixel.isDisparityValid(disp)) {
				costImg.at<uint16_t>(y, x) = getMaxCost();
				validDisparitiesMask.at<uint8_t>(y,x) = 0;
			} else {
				uint16_t val = valueAt(y, x, dispImg(y, x));
				costImg.at<uint16_t>(y, x) = val;
				if (val < getMaxCost())
					validDisparitiesMask(y,x) = 255;
				else
					validDisparitiesMask(y,x) = 0;
			}
		}
	}
}

void DisparitySpaceImage::writeCostAt(const cv::Mat& dispImg,
		const std::string filename,
		const cv::Mat_<uint8_t> mask) const {
	assert(mask.dims == 0 || mask.size() == dispImg.size());
	assert(dispImg.size() == baseImg.size());

	if (params.verbosity & writeDebugConsoleOutput)
		std::cout << "Writing image: " << filename << " ..." << std::flush;
	cv::Mat_<uint8_t> red, green, blue;
	cv::Mat_<cv::Vec3b> pngImg;
	cv::Mat_<int16_t> dispIntImg;
	cv::Mat_<uint16_t> costImg;
	cv::Mat_<uint8_t> validDisparitiesMask;

	dispImg.convertTo(dispIntImg, CV_16S);
	getCostAt(dispIntImg, costImg, validDisparitiesMask);

	// filter the invalid costs to get a more meaningful image
	costImg.setTo(0, validDisparitiesMask == 0);

	if (params.verbosity & writeDebugConsoleOutput) {
		double min, max;
		cv::Point minPoint, maxPoint;
		cv::minMaxLoc(costImg, &min, &max, &minPoint, &maxPoint);
		std::cout << " (maximum: " << max << " at point: " << maxPoint << ", disparity: " << dispIntImg(maxPoint.y, maxPoint.x) << ")" << std::flush;
	}

	cv::normalize(costImg, green, 0, 255, cv::NORM_MINMAX, CV_8U, validDisparitiesMask);
	green &= validDisparitiesMask;

	blue = ~alreadyComputedPixels;
	red = alreadyComputedPixels & ~validDisparitiesMask;

	blue += green;
	red += green;

	cv::Mat channels[] = { blue, green, red };
	cv::merge(channels, 3, pngImg);
	cv::imwrite(filename, pngImg);
	std::cout << " done." << std::endl;
}

void DisparitySpaceImage::writeCostAtWTAImg(const std::string filename,
		const cv::Mat_<uint8_t> mask) {
	writeCostAt(getIntWTAImage(), filename, mask);
}

void DisparitySpaceImage::writeDSI(int fromX, int toX, int fromY, int toY) const {
	assert(fromX >= 0);
	assert(fromY >= 0);

	if (toX < 0)
		toX = getCols();
	if (toY < 0)
		toY = getRows();
	for (int y = fromY; y < toY; ++y) {
		for (int x = fromX; x < toX; ++x) {
			std::cout << "(" << y << "," << x << "): ";
			const DSIPixel& pixel = at(y, x);
			for (int disp = minDisparity; disp < maxDisparity; ++disp) {
				std::cout << pixel.valueAt(disp) << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
}
