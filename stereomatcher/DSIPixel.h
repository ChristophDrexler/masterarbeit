/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef DSIPIXEL_H_
#define DSIPIXEL_H_

#include <algorithm>
#include <cassert>
#include <limits>
#include <vector>

#include "Parameters.h"

class DSIPixel {
private:
	int16_t minDisparity;
	std::vector<datatype> values;
public:
	DSIPixel() : minDisparity(getInvalidDisparity()) {}

	DSIPixel(int16_t minDisparity, int16_t maxDisparity, datatype initialValue) :
			minDisparity(minDisparity) {
		assert(minDisparity < maxDisparity);

		values = std::vector<datatype>(maxDisparity - minDisparity, initialValue);
	}

	void resize(int16_t minDisp, int16_t maxDisp, datatype initialValue) {
		assert(minDisp < maxDisp);

		minDisparity = minDisp;
		values.resize(maxDisp - minDisp, initialValue);
	}
	datatype valueAt(int disp) const {
		if (disp < minDisparity || disp >= getMaxDisparity())
			return getMaxCost();
		return values[disp-minDisparity];
	}
	std::vector<datatype>::reference at(int disp) {
		assert(disp >= minDisparity);
		assert(disp < getMaxDisparity());
		return values[disp-minDisparity];
	}
	void getLeastCost(datatype& leastCost, int16_t& disparity) const {
		if (values.size() == 0) {
			leastCost = getMaxCost();
			disparity = getInvalidDisparity();
		}
		std::vector<datatype>::const_iterator iter;
		iter = std::min_element(values.begin(), values.end());
		leastCost = *iter;
		disparity = iter - values.begin() + minDisparity;
	}
	datatype getLeastCost() const {
		if (values.size() == 0)
			return getMaxCost();
		std::vector<datatype>::const_iterator iter;
		iter = std::min_element(values.begin(), values.end());
		return *iter;
	}
	datatype getMaxCost() const { return std::numeric_limits<datatype>::max(); }
	int16_t getInvalidDisparity() const { return std::numeric_limits<int16_t>::max(); }
	int16_t getMinDisparity() const { return minDisparity; }
	int16_t getMaxDisparity() const { return minDisparity + values.size(); }
	bool isDisparityValid(int disp) const { return (disp >= getMinDisparity() && disp < getMaxDisparity()); }
	bool isValid() const { return (minDisparity != getInvalidDisparity()); }
};

#endif /* DSIPIXEL_H_ */
