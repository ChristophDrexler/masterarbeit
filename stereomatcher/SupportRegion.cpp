/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include "SupportRegion.h"
#include "Timer.h"

void writeImage(const std::string& filename, const cv::Mat& img) {
	cv::Mat pngImg;
	cv::normalize(img, pngImg, 0, 255, cv::NORM_MINMAX, CV_8U);
	cv::imwrite(filename, pngImg);
}

void SupportRegion::constructSupportRegion() {
	if (rightArm.data != NULL && leftArm.data != NULL && lowerArm.data != NULL
			&& upperArm.data != NULL)
		return;

	Timer timer;
	timer.start();

	leftArm.create(img.size());
	rightArm.create(img.size());
	upperArm.create(img.size());
	lowerArm.create(img.size());

	for (int y = 0; y < img.getRows(); ++y) {
		for (int x = 0; x < img.getCols(); ++x) {
			leftArm(y,x) = constructArm(LEFT, y, x);
			rightArm(y,x) = constructArm(RIGHT, y, x);
			upperArm(y,x) = constructArm(UP, y, x);
			lowerArm(y,x) = constructArm(DOWN, y, x);
		}
	}

	if (params.verbosity & writeDebugImages) {
		writeImage("debug-supportRegion-rightArm.png", rightArm);
		writeImage("debug-supportRegion-leftArm.png", leftArm);
		writeImage("debug-supportRegion-lowerArm.png", lowerArm);
		writeImage("debug-supportRegion-upperArm.png", upperArm);
	}

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for constructing the support region: "
				<< timer.stop() << " s" << std::endl;
}

uint8_t SupportRegion::constructArm(Direction dir, int y, int x) {
	if (!img.isValid(y, x))
		return 0;

	int endpointX = x;
	int endpointY = y;
	uint8_t distance = 0;
	bool okay = true;
	int previousColorDistance = 0;
	while (okay && distance < 255) {
		++distance;
		goToNextPoint(dir, endpointY, endpointX);
		if (!img.isValid(endpointY, endpointX)) {
			okay = false;
		} else {
			int colorDistance = getColorDistance(img.at(y, x),
					img.at(endpointY, endpointX));
			if (colorDistance >= params.cross.t1) { //Rule 1a
				okay = false;
			} else if (previousColorDistance >= params.cross.t1) { // Rule 1b
				okay = false;
			} else if (distance >= params.cross.l1) { // Rule 2
				okay = false;
			} else if (distance >= params.cross.l2
					&& colorDistance >= params.cross.t2) {
				okay = false;
			}
			previousColorDistance = colorDistance;
		}
	}
	return distance - 1;
}

inline void SupportRegion::goToNextPoint(Direction dir, int& endpointY,
		int&endpointX) {
	switch (dir) {
	case LEFT:
		--endpointX;
		break;
	case RIGHT:
		++endpointX;
		break;
	case UP:
		--endpointY;
		break;
	case DOWN:
		++endpointY;
		break;
	default:
		break;
	}
}

inline int SupportRegion::getColorDistance(const cv::Vec3b& pixel,
		const cv::Vec3b& endpoint) const {
	int colorDistance = std::max(std::abs(endpoint[0] - pixel[0]),
			std::abs(endpoint[1] - pixel[1]));
	return std::max(colorDistance, std::abs(endpoint[2] - pixel[2]));
}
