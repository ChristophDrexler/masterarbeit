/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <iostream>
#include <typeinfo>

#include "DSIPixel.h"
#include "Error.h"
#include "SGMAggregation.h"
#include "Timer.h"

void SGMAggregation::aggregate() {
	Timer timer;
	timer.start();

	if (params.verbosity & writeDebugConsoleOutput) {
		std::cout << "SGM penalty1:\t" << params.getSGMPenalty1()
						<< "; SGM penalty2:\t" << params.getSGMPenalty2()
						<< "; window width:\t" << params.census.width
						<< "; window height:\t" << params.census.height
				<< std::endl;
	}

	aggregateOneDirection(N2S);
	aggregateOneDirection(NE2SW);
	aggregateOneDirection(E2W);
	aggregateOneDirection(SE2NW);
	aggregateOneDirection(S2N);
	aggregateOneDirection(SW2NE);
	aggregateOneDirection(W2E);
	aggregateOneDirection(NW2SE);

	dest.setValidPixelsToComputedPixels();

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time needed for the SGM aggregation: "
				<< timer.stop() << " s" << std::endl;

	if (params.verbosity & writeDebugImages)
		dest.writeWTAImage("WTA-SGM.png");
}

void SGMAggregation::aggregateOneDirection(Direction dir) {
	Timer timer;
	timer.start();

	switch (dir) {
	case W2E:
		aggregateOneDirection(0, 0, 1, 0, 0, 1);
		break;
	case N2S:
		aggregateOneDirection(0, 0, 0, 1, 1, 0);
		break;
	case E2W:
		aggregateOneDirection(dest.getCols() - 1, 0, -1, 0, 0, 1);
		break;
	case S2N:
		aggregateOneDirection(0, dest.getRows() - 1, 0, -1, 1, 0);
		break;
	case NW2SE:
		aggregateOneDirection(0, 0, 1, 1, 0, 1);
		aggregateOneDirection(1, 0, 1, 1, 1, 0);
		break;
	case SW2NE:
		aggregateOneDirection(0, 0, 1, -1, 0, 1);
		aggregateOneDirection(1, dest.getRows() - 1, 1, -1, 1, 0);
		break;
	case NE2SW:
		aggregateOneDirection(0, 0, -1, 1, 1, 0);
		aggregateOneDirection(dest.getCols() - 1, 1, -1, 1, 0, 1);
		break;
	case SE2NW:
		aggregateOneDirection(0, dest.getRows() - 1, -1, -1, 1, 0);
		aggregateOneDirection(dest.getCols() - 1, dest.getRows() - 2, -1, -1, 0, -1);
		break;
	default:
		throw Error("Direction does not exist: " + dir);
		break;
	}

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for doing the " << getString(dir) << " aggregation: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages) {
		cv::Mat origMask = dest.getComputedPixelsMat().clone();
		dest.setValidPixelsToComputedPixels();
		std::stringstream filename;
		filename << "debug-WTA-SGM-" << params.counter << "-" << (int) dir << "-" << getString(dir) << ".png";
		dest.writeWTAImage(filename.str());
		filename.str("");
		filename << "debug-costAtWTA-SGM-" << params.counter << "-" << (int) dir << "-"
				<< getString(dir) << ".png";
		dest.writeCostAt(dest.getIntWTAImage(), filename.str());
		dest.getComputedPixelsMat() = origMask;
	}
}

void SGMAggregation::aggregateOneDirection(int startX, int startY, int toNextX,
		int toNextY, int toNextScanLineX, int toNextScanLineY) {
	assert(baseAddedIntensitiesImg.type() == CV_16SC1);

	uint16_t maxCost = src.getMaxCost();
	uint16_t p1 = params.getSGMPenalty1();
	uint16_t p2 = params.getSGMPenalty2();

	cv::Mat_<uint8_t>& alreadyAggregatedPixels = dest.getComputedPixelsMat();
	for (; startX >= 0 && startY >= 0 && startX < src.getCols() && startY < src.getRows();
			startX += toNextScanLineX, startY += toNextScanLineY) {
		bool previousPixelIsValid = false;
		DSIPixel previousPixel, nextPixel;
		int16_t previousIntensity, nextIntensity;
		uint16_t previousMin;
		uint16_t nextMin = maxCost; // means: we do not know the minimum aggregated cost of the previous pixel;
		for (int x = startX, y = startY;
				x >= 0 && y >= 0 && x < src.getCols() && y < src.getRows();
				x += toNextX, y += toNextY) {
			if (alreadyAggregatedPixels(y,x) > 0) {
				previousPixelIsValid = true;
				nextMin = maxCost; // means: we do not know the minimum aggregated cost of the previous pixel;
			} else { // (alreadyAggregatedPixels(y,x) == 0)
				DSIPixel& destPixel = dest.at(y, x);
				DSIPixel& srcPixel = src.at(y, x);
				if (destPixel.isValid()) {
					uint16_t computedP2;
					if (previousPixelIsValid) {
						if (nextMin < maxCost) { // previous pixel has been computed now
							previousPixel = nextPixel;
							previousMin = nextMin;
						} else { // previous pixel has been computed before
							previousPixel = dest.at(y - toNextY, x - toNextX);
							previousMin = previousPixel.getLeastCost();
						}
						previousIntensity = nextIntensity;
						nextIntensity = baseAddedIntensitiesImg(y,x);
						computedP2 = computePenalty2(p1, p2,
								previousIntensity, nextIntensity);
					} else { // ! previousPixelIsValid
						nextIntensity = baseAddedIntensitiesImg(y,x);
					}
					nextPixel = srcPixel;
					nextMin = maxCost; // new minimum has to be computed
					for (int disp = srcPixel.getMinDisparity();
							disp < srcPixel.getMaxDisparity(); ++disp) {
						if (previousPixelIsValid) {
							uint16_t val1 = cv::saturate_cast<uint16_t>(
									std::min(previousPixel.valueAt(disp-1),
											previousPixel.valueAt(disp+1)) + p1);
							uint16_t val2 = cv::saturate_cast<uint16_t>(previousMin + computedP2);
							uint16_t min = std::min(previousPixel.valueAt(disp), val1);
							min = std::min(min, val2);
							nextPixel.at(disp) = cv::saturate_cast<uint16_t>(
									srcPixel.at(disp) + min - previousMin
							);
						}
						destPixel.at(disp) = cv::saturate_cast<uint16_t>(
								destPixel.at(disp) + nextPixel.at(disp)
						);
						if (nextMin > nextPixel.at(disp))
							nextMin = nextPixel.at(disp);
					}
					previousPixelIsValid = true;
				} else { // ! destPixel.isValid()
					previousPixelIsValid = false;
					nextMin = maxCost; // means: we do not know the minimum aggregated cost of the previous pixel;
				}
			} // !(alreadyAggregatedPixels(y,x) > 0)
		} // for
	} // for
}

uint16_t SGMAggregation::computePenalty2(uint16_t p1, uint16_t p2,
		int16_t previousIntensity, int16_t nextIntensity) {
	uint16_t computedP2;
	if (nextIntensity == previousIntensity)
		computedP2 = p2;
	else
		computedP2 = cv::saturate_cast<uint16_t>(
				p2 / std::abs(nextIntensity - previousIntensity));
	if (computedP2 <= p1)
		computedP2 = p1 + 1; // see Hermann et al. 2009, 635

	return computedP2;
}

std::string SGMAggregation::getString(Direction dir) {
	switch (dir) {
	case N2S:
		return "N2S";
		break;
	case NE2SW:
		return "NE2SW";
		break;
	case E2W:
		return "E2W";
		break;
	case SE2NW:
		return "SE2NW";
		break;
	case S2N:
		return "S2N";
		break;
	case SW2NE:
		return "SW2NE";
		break;
	case W2E:
		return "W2E";
		break;
	case NW2SE:
		return "NW2SE";
		break;
	default:
		throw Error("Direction does not exist: " + dir);
		break;
	}
	return "Error";
}
