/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include <string>
#include <opencv2/opencv.hpp>

class Image {
private:
	cv::Mat_<cv::Vec3b> img;
	cv::Mat_<uint8_t> mask;
	cv::Mat_<int16_t> addedIntensitiesMat;
	cv::Mat_<int16_t> xGradientMat, yGradientMat;
public:
	Image() {}
	Image(const std::string& filename, const std::string& maskFilename);

	int getRows()           const { return img.rows;    }
	int getCols()           const { return img.cols;    }
	const cv::Mat& getMat() const { return img;         }
	cv::Size size()         const { return img.size();  }
	size_t total()          const { return img.total(); }
	const cv::Mat_<uint8_t>& getValidPixelsMask() const { return mask; }

	inline bool isValid(int y, int x) const {
		return x >= 0 && x < img.cols && y >= 0 && y < img.rows
				&& mask(y,x) > 0;
	}

	const cv::Mat_<int16_t>& getAddedIntensitiesMat();
	const cv::Mat_<int16_t>& getXGradientMat();
	const cv::Mat_<int16_t>& getYGradientMat();

	cv::Vec3b& at(cv::Point point) { return img.at<cv::Vec3b>(point); }
	const cv::Vec3b& at(cv::Point point) const { return img.at<cv::Vec3b>(point); }
	cv::Vec3b& at(int y, int x) { return img.at<cv::Vec3b>(y,x); }
	const cv::Vec3b& at(int y, int x) const { return img.at<cv::Vec3b>(y,x); }
private:
	void addIntensities();
	void computeXGradient();
	void computeYGradient();
};

#endif /* IMAGE_H_ */
