/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef AGGREGATION_H_
#define AGGREGATION_H_

#include <opencv2/opencv.hpp>

#include "DisparitySpaceImage.h"
#include "Error.h"
#include "Image.h"
#include "Parameters.h"

class Aggregation {
protected:
	const Parameters& params;
	DisparitySpaceImage& src;
	DisparitySpaceImage& dest;
	Image& baseImg;
public:
	Aggregation(const Parameters& p,
			DisparitySpaceImage& src,
			DisparitySpaceImage& dest,
			Image& baseImg) :
			params(p), src(src), dest(dest), baseImg(baseImg) {}
	virtual ~Aggregation() {}
	virtual void aggregate() {
		throw Error("generic aggregation method");
	}
};

#endif /* AGGREGATION_H_ */
