/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include <stdint.h>
#include <string>

typedef uint16_t datatype;

class Parameters;

enum VerbosityEnum {
	noOutput				= 0,
	writeBasicConsoleOutput	= 1,
	writeTimesToConsole		= 2,
	writeDebugConsoleOutput	= 4,
	writeOnlyDisparityImage	= 16,
	writeImportantImages	= 32,
	writeDebugImages		= 64,
	writeHistogramImages	= 128
};

enum CostFunctionEnum {
	functionAD,
	functionADCensus,
	functionADCensusGradient,
	functionCensus,
	functionCensusOfGradient,
	functionGradientCensus,
	functionGradientDiff
};

enum AggregationMethodEnum {
	aggregationCrossBased,
	aggregationCrossSGM,
	aggregationSGM
};

class Parameters {
public:
	bool debug; //for debugging purposes

	int counter;
	int minDisparity, maxDisparity;
	int verbosity;
	std::string baseValidityFileName, matchValidityFileName;
	std::string baseFeaturesFileName, matchFeaturesFileName;
	CostFunctionEnum costFunction;
	int threshold1, threshold2;
	AggregationMethodEnum aggregationMethod;
	std::string baseImgFileName, matchImgFileName;
	std::string groundTruthFileName;
	std::vector<std::string> maskFileNames;
	double errorThreshold;
	double percentageToCompute;
	int maxRounds;

	struct CensusParameters {
		int width;
		int height;
		int step;
		bool flagMCT;
	} census;

	struct CrossBasedAggregationParameters {
		int t1;
		int t2;
		int l1;
		int l2;
	} cross;

	struct SGMParameters {
		int penalty1;
		int penalty2;
	} sgm;

	Parameters() {
		debug					= false;
		counter					= 0;

		minDisparity			= std::numeric_limits<int>::max();
		maxDisparity			= std::numeric_limits<int>::min();
		verbosity				= writeOnlyDisparityImage;
		baseImgFileName			= "";
		matchImgFileName		= "";
		baseValidityFileName	= "";
		matchValidityFileName	= "";
		costFunction			= functionCensus;
		aggregationMethod		= aggregationSGM;
		groundTruthFileName		= "";
		errorThreshold			= 1.0;
		percentageToCompute		= 0.8;
		maxRounds				= 5;

		threshold1		= -1; // -1 means: not defined by console option
		threshold2		= -1;
		census.width	= -1;
		census.height	= -1;
		census.step		= -1;
		census.flagMCT	= false;

		cross.t1		= 30;
		cross.t2		= 6;
		cross.l1		= 34;
		cross.l2		= 17;

		sgm.penalty1		= -1;
		sgm.penalty2		= -1;
	}
	void setCensusParameters(const int width, const int height, const int step) {
		// set only if not manually set by a command line option
		if (census.width < 0)
			census.width = width;
		if (census.height < 0)
			census.height = height;
		if (census.step < 0)
			census.step = step;
	}
	void setThreshold(const int threshold1, const int threshold2 = -1) {
		// set only if not manually set by a command line option
		if (this->threshold1 < 0)
			this->threshold1 = threshold1;
		if (this->threshold2 < 0)
			this->threshold2 = threshold2;
	}
	void setSGMParameters(const int penalty1, const int penalty2) {
		// set only if not manually set by a command line option
		if (sgm.penalty1 < 0)
			sgm.penalty1 = penalty1;
		if (sgm.penalty2 < 0)
			sgm.penalty2 = penalty2;
	}
	int getSGMPenalty1() const { return sgm.penalty1; }
	int getSGMPenalty2() const { return sgm.penalty2; }
};

#endif /* PARAMETERS_H_ */
