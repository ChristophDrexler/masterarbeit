/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

/*
 * This utility is based on a OpenCV tutorial,
 * see: http://opencv.itseez.com/doc/tutorials/features2d/feature_flann_matcher/feature_flann_matcher.html
 */

#include <fstream>
#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>

void readme();

/** @function main */
int main(int argc, char** argv) {
	if (argc != 4) {
		readme();
		return -1;
	}

	cv::Mat img_1 = cv::imread(argv[1], cv::IMREAD_COLOR);
	cv::Mat img_2 = cv::imread(argv[2], cv::IMREAD_COLOR);
	std::string detectorType = argv[3];

	if (!img_1.data || !img_2.data) {
		std::cout << " --(!) Error reading images " << std::endl;
		return -1;
	}

	//-- Step 1: Detect the keypoints using the specified Detector
	cv::Ptr<cv::FeatureDetector> detector = cv::FeatureDetector::create(detectorType);

	std::vector<cv::KeyPoint> keypoints_1, keypoints_2;

	detector->detect(img_1, keypoints_1);
	detector->detect(img_2, keypoints_2);

	//-- Step 2: Calculate descriptors (feature vectors)
	cv::Ptr<cv::DescriptorExtractor> extractor = cv::DescriptorExtractor::create("SURF");

	cv::Mat descriptors_1, descriptors_2;

	extractor->compute(img_1, keypoints_1, descriptors_1);
	extractor->compute(img_2, keypoints_2, descriptors_2);

	//-- Step 3: Matching descriptor vectors using FLANN matcher
	cv::FlannBasedMatcher matcher;
	std::vector<cv::DMatch> matches;
	matcher.match(descriptors_1, descriptors_2, matches);

	double max_dist = 0;
	double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	for (int i = 0; i < descriptors_1.rows; i++) {
		double dist = matches[i].distance;
		if (dist < min_dist)
			min_dist = dist;
		if (dist > max_dist)
			max_dist = dist;
	}

	std::cout << "-- Max dist : " << max_dist << std::endl;
	std::cout << "-- Min dist : " << min_dist << std::endl;

	//-- Store only "good" matches
	std::vector<cv::DMatch> good_matches;

	for (int i = 0; i < descriptors_1.rows; i++) {
//		if( matches[i].distance < 2*min_dist )
//		if (matches[i].distance < 0.5 * (min_dist + max_dist)) {
			good_matches.push_back(matches[i]);
//		}
	}

	std::ofstream leftFile("leftPoints.txt");
	std::ofstream rightFile("rightPoints.txt");
	leftFile.precision(10);
	rightFile.precision(10);
	std::cout.precision(9);
	for (unsigned int i = 0; i < good_matches.size(); i++) {
		std::cout << "-- Good Match [" << i << "] ";
		std::cout << "Keypoint 1: " << good_matches[i].queryIdx << "  -- ";
		std::cout << "Keypoint 2: " << good_matches[i].trainIdx << std::endl;
		std::cout << keypoints_1[good_matches[i].queryIdx].pt.x << " ";
		std::cout << keypoints_1[good_matches[i].queryIdx].pt.y << " <---> ";
		std::cout << keypoints_2[good_matches[i].trainIdx].pt.x << " ";
		std::cout << keypoints_2[good_matches[i].trainIdx].pt.y << std::endl;

		leftFile << keypoints_1[good_matches[i].queryIdx].pt.x << " ";
		leftFile << keypoints_1[good_matches[i].queryIdx].pt.y;
		leftFile << std::endl;
		rightFile << keypoints_2[good_matches[i].trainIdx].pt.x << " ";
		rightFile << keypoints_2[good_matches[i].trainIdx].pt.y;
		rightFile << std::endl;
	}
	leftFile.close();
	rightFile.close();

	return EXIT_SUCCESS;
}

/** @function readme */
void readme() {
	std::cout << " Usage: ./featurematcher <img1> <img2> \"DetectorType\""
			<< std::endl;
	std::cout << " where DetectorType is one of: ";
	std::cout << "FAST STAR SIFT SURF ORB MSER GFTT HARRIS Dense SimpleBlob"
			<< std::endl;
}
