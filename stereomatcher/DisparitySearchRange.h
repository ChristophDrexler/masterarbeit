/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef DISPARITYSEARCHRANGE_H_
#define DISPARITYSEARCHRANGE_H_

#include <map>
#include <set>
#include <opencv2/opencv.hpp>

#include "Parameters.h"
#include "SupportRegion.h"

struct Feature {
	Feature(double disp, cv::Point2d p) :
		disparity(disp), point(p) {}
	bool operator < (const Feature& f2) const {
		if (disparity != f2.disparity)
			return disparity < f2.disparity;
		if (point.x != f2.point.x)
			return point.x < f2.point.x;
		return point.y < f2.point.y;
	}
	bool operator != (const Feature& f2) const {
		return point != f2.point || disparity != f2.disparity;
	}
	double disparity;
	cv::Point2d point;
};

struct Edge {
	Edge(Feature f1, Feature f2) :
		fromFeature(f1), toFeature(f2) {}
	bool operator < (const Edge& e2) const {
		if (fromFeature != e2.fromFeature)
			return fromFeature < e2.fromFeature;
		return toFeature < e2.toFeature;
	}
	Feature fromFeature, toFeature;
};

class DisparitySearchRange {
private:
	const Parameters& params;
	const Image& baseImg;
	cv::Mat_<int16_t> minDisp, maxDisp;
	std::set<Feature> featurePoints;
	const SupportRegion& support;
public:
	DisparitySearchRange(const Parameters& p,
			const Image& baseImg,
			const SupportRegion& support) :
			params(p), baseImg(baseImg), support(support) {}
	void calculateByInterpolatingFeatureDisparitiesAndPropagation();
	void findMissingDispSearchRanges();
	void setTo(const cv::Mat_<int16_t> wtaImg, const cv::Mat_<uint8_t> mask);
	void writeFeaturePoints(const std::string& filename, cv::Mat_<float> groundTruth) const;

	int getRows() const { return baseImg.getRows(); }
	int getCols() const { return baseImg.getCols(); }
	const cv::Mat_<int16_t>& getMinDisp() const { return minDisp; }
	const cv::Mat_<int16_t>& getMaxDisp() const { return maxDisp; }
private:
	void initializeMinDisp();
	void initializeMaxDisp();
	void fetchFeaturePoints();
	void setAndInterpolateBetweenNeighbouringFeatures();
	void lookForNeighbours(std::set<Edge>& neighbours);
	int getMaxColorDistance(const Feature& f1, const Feature& f2);
	int getMaxColorDistance(cv::Vec3b pixel1, cv::Vec3b pixel2);
	void setAndInterpolateBetweenFeaturePoints();
	void interpolateBetweenFeaturePoints(const Feature& fromFeature, const Feature& toFeature);
	void propagateOnce();
	void propagate(int y, int x);
	void propagate(int y, int x, const cv::Mat_<int16_t>& srcMinDisp, const cv::Mat_<int16_t> srcMaxDisp);
	void propagate(int y, int x, const cv::Mat_<int16_t>& srcMinDisp, const cv::Mat_<int16_t> srcMaxDisp, int nrOfPixels);

	bool extendRangeToMinDisp(int y, int x, int disp);
	bool extendRangeToMaxDisp(int y, int x, int disp);
	bool extendRange(int y, int x, int disp);
	bool extendRange(int y, int x, int min, int max);

	void extendMinAndMaxDisparities(int threshold, int nrOfDisparites);
	void extendRangeToAllDirections(int nrOfPixels);
	void extendRangeTo(int minRange);

	void writeMinMaxDisp(const std::string& nameSuffix) const;
	void writeImg(const std::string& name, const cv::Mat& img) const;
};

#endif /* DISPARITYSEARCHRANGE_H_ */
