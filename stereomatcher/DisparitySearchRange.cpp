/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <fstream>
#include <limits>
#include <map>

#include "DisparitySearchRange.h"
#include "Error.h"
#include "Evaluation.h"
#include "Timer.h"

void DisparitySearchRange::calculateByInterpolatingFeatureDisparitiesAndPropagation() {
	Timer timer;
	timer.start();

	initializeMinDisp();
	initializeMaxDisp();
	fetchFeaturePoints();

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for fetching the feature points: "
				<< timer.stop() << " s" << std::endl;
	timer.start();

	setAndInterpolateBetweenNeighbouringFeatures();

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for interpolating between neighbouring features: "
				<< timer.stop() << " s" << std::endl;
}

void DisparitySearchRange::findMissingDispSearchRanges() {
	Timer timer;

	if (params.verbosity & writeDebugImages) writeMinMaxDisp("-0-initialValues");

	timer.start();
	minDisp -= 10;
	maxDisp += 10;
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for extending the minimum and maximum disparities: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages) writeMinMaxDisp("-1-extendedMinMax");

	timer.start();
	propagateOnce();
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for propagating the disparities: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages) writeMinMaxDisp("-2-propagated");

	timer.start();
	propagateOnce();
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for propagating the disparities: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages) writeMinMaxDisp("-3-propagated");

	timer.start();
	propagateOnce();
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for propagating the disparities: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages) writeMinMaxDisp("-4-propagated");

	timer.start();
	extendRangeToAllDirections(30);
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for extending the disparities to all directions: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages) writeMinMaxDisp("-5-extendedToAllDirections");
}

void DisparitySearchRange::setTo(const cv::Mat_<int16_t> wtaImg, const cv::Mat_<uint8_t> mask) {
	assert(wtaImg.size() == baseImg.size());

	initializeMinDisp();
	wtaImg.copyTo(minDisp, mask);
	initializeMaxDisp();
	cv::add(wtaImg, 1, maxDisp, mask);
}

void DisparitySearchRange::writeFeaturePoints(const std::string& filename, cv::Mat_<float> groundTruth) const {
	cv::Mat baseImgCopy = baseImg.getMat().clone();
	cv::Mat baseImgCopy2 = baseImg.getMat().clone();

	std::set<Feature>::iterator it;
	for (it = featurePoints.begin(); it != featurePoints.end(); ++it) {
		double disp = it->disparity;
		cv::Point point = it->point;
		int diff = std::abs(groundTruth.at<float>(point) - disp);
		if (diff > 2) {
			cv::circle(baseImgCopy, point, diff, cv::Scalar(0, 0, 255), 1);
			cv::circle(baseImgCopy2, point, 3, cv::Scalar(0, 0, 255), 3);
		} else {
			cv::circle(baseImgCopy, point, std::max(diff,3), cv::Scalar(0, 255, 0), 1);
			cv::circle(baseImgCopy2, point, 3, cv::Scalar(0, 255, 0), 3);
		}
	}
	cv::imwrite(filename, baseImgCopy);
	cv::imwrite(filename.substr(0, filename.size()-4) + "-largepoints" + filename.substr(filename.size()-4, 4),
			baseImgCopy2);
}

void DisparitySearchRange::initializeMinDisp() {
	minDisp.create(baseImg.size());
	minDisp.setTo(cv::Scalar(std::numeric_limits<int16_t>::max()));
}

void DisparitySearchRange::initializeMaxDisp() {
	maxDisp.create(baseImg.size());
	maxDisp.setTo(cv::Scalar(std::numeric_limits<int16_t>::min()));
}

void DisparitySearchRange::fetchFeaturePoints() {
	std::ifstream baseFeatures(params.baseFeaturesFileName.c_str());
	std::ifstream matchFeatures(params.matchFeaturesFileName.c_str());
	if (baseFeatures.fail() || matchFeatures.fail())
		throw Error("Error reading the feature coordinates!");
	while (baseFeatures.good() && matchFeatures.good()) {
		cv::Point2d basePoint, matchPoint;
		baseFeatures >> basePoint.x;
		baseFeatures >> basePoint.y;
		matchFeatures >> matchPoint.x;
		matchFeatures >> matchPoint.y;
		double disp = matchPoint.x - basePoint.x;
		if (basePoint.x != 0 || basePoint.y != 0 || disp != 0) {
			featurePoints.insert(Feature(disp, basePoint));
		}
	}
}

void DisparitySearchRange::setAndInterpolateBetweenNeighbouringFeatures() {
	std::set<Edge> neighbours;
	lookForNeighbours(neighbours);
	std::set<Edge>::iterator it;
	for (it = neighbours.begin(); it != neighbours.end(); ++it) {
		interpolateBetweenFeaturePoints(it->fromFeature, it->toFeature);
	}
}

void DisparitySearchRange::lookForNeighbours(std::set<Edge>& neighbours) {
	std::set<Feature>::iterator it1;
	std::set<Feature>::iterator it2;
	for (it1 = featurePoints.begin(); it1 != featurePoints.end(); ++it1) {
		std::multimap<double, Feature> nextNeighbours;
		for (it2 = featurePoints.begin(); it2 != featurePoints.end(); ++it2) {
			if (it1 != it2) {
				double distance = getMaxColorDistance(*it1, *it2);
				if (nextNeighbours.size() < 5) {
					nextNeighbours.insert(
							std::pair<double, Feature>(distance, *it2));
				} else {
					if (distance < nextNeighbours.rbegin()->first) {
						nextNeighbours.insert(std::pair<double, Feature>(distance, *it2));
						nextNeighbours.erase(--nextNeighbours.end());
					}
				}
			}
		}

		std::multimap<double, Feature>::iterator it;
		for (it = nextNeighbours.begin(); it != nextNeighbours.end(); ++it) {
			if (neighbours.find(Edge(*it1, it->second)) == neighbours.end()
					&& neighbours.find(Edge(it->second, *it1)) == neighbours.end()) {
				neighbours.insert(Edge(*it1, it->second));
			}
		}
	}
}

inline int DisparitySearchRange::getMaxColorDistance(const Feature& f1,
		const Feature& f2) {
	int distance = 0;
	int nrOfSteps = cv::saturate_cast<int>(
			std::max(std::abs(f2.point.x - f1.point.x),
					std::abs(f2.point.y - f1.point.y)));
	if (nrOfSteps > 0) {
		double xStep = (f2.point.x - f1.point.x) / nrOfSteps;
		double yStep = (f2.point.y - f1.point.y) / nrOfSteps;
		cv::Vec3b pixel = baseImg.at(f1.point);
		for (int i = 0; i <= nrOfSteps; ++i) {
			cv::Vec3b previousPixel = pixel;
			pixel = baseImg.at(cv::Point(f1.point.x + i * xStep, f1.point.y + i * yStep));
			distance = std::max(distance, getMaxColorDistance(pixel, previousPixel));
		}
	}
	return distance;
}

inline int DisparitySearchRange::getMaxColorDistance(cv::Vec3b pixel1, cv::Vec3b pixel2) {
	int distance = std::max(std::abs(pixel1[0] - pixel2[0]), std::abs(pixel1[1] - pixel2[1]));
	return std::max(distance, std::abs(pixel1[2] - pixel2[2]));
}

void DisparitySearchRange::setAndInterpolateBetweenFeaturePoints() {
	std::set<Feature>::iterator it1;
	std::set<Feature>::iterator it2;
	for (it1 = featurePoints.begin(); it1 != --featurePoints.end(); ++it1) {
		it2 = it1;
		for (++it2; it2 != featurePoints.end(); ++it2) {
			interpolateBetweenFeaturePoints(*it1, *it2);
		}
	}
}

void DisparitySearchRange::interpolateBetweenFeaturePoints(
		const Feature& fromFeature, const Feature& toFeature) {
	double disp1 = fromFeature.disparity;
	double disp2 = toFeature.disparity;
	int nrOfSteps = cv::saturate_cast<int>(
			std::max(std::abs(toFeature.point.x - fromFeature.point.x),
					std::abs(toFeature.point.y - fromFeature.point.y)));
	if (nrOfSteps > 0) {
		double xStep = (toFeature.point.x - fromFeature.point.x) / nrOfSteps;
		double yStep = (toFeature.point.y - fromFeature.point.y) / nrOfSteps;
		double dispStep = (double) (disp2 - disp1) / nrOfSteps;
		for (int i = 0; i <= nrOfSteps; ++i) {
			extendRange(cv::saturate_cast<int>(fromFeature.point.y + i * yStep),
					cv::saturate_cast<int>(fromFeature.point.x + i * xStep),
					cv::saturate_cast<int16_t>(disp1 + i * dispStep));
		}
	}
}

void DisparitySearchRange::propagateOnce() {
	cv::Mat srcMinDisp = minDisp.clone();
	cv::Mat srcMaxDisp = maxDisp.clone();
	for (int y = 0; y < baseImg.getRows(); ++y) {
		for (int x = 0; x < baseImg.getCols(); ++x) {
			propagate(y, x, srcMinDisp, srcMaxDisp);
		}
	}
}

void DisparitySearchRange::propagate(int y, int x) {
	propagate(y, x, minDisp, maxDisp);
}

void DisparitySearchRange::propagate(int y, int x, const cv::Mat_<int16_t>& srcMinDisp, const cv::Mat_<int16_t> srcMaxDisp) {
	int16_t min = srcMinDisp(y,x);
	int16_t max = srcMaxDisp(y,x);
	cv::Mat_<uint8_t> leftArm = support.getLeftArm();
	for (int i = 1; i <= leftArm(y, x); ++i) {
		extendRange(y, x - i, min, max);
	}
	cv::Mat_<uint8_t> rightArm = support.getRightArm();
	for (int i = 1; i <= rightArm(y, x); ++i) {
		extendRange(y, x + i, min, max);
	}
	cv::Mat_<uint8_t> upperArm = support.getUpperArm();
	for (int i = 1; i <= upperArm(y, x); ++i) {
		extendRange(y - i, x, min, max);
	}
	cv::Mat_<uint8_t> lowerArm = support.getLowerArm();
	for (int i = 1; i <= lowerArm(y, x); ++i) {
		extendRange(y + i, x, min, max);
	}
}

void DisparitySearchRange::propagate(int y, int x, const cv::Mat_<int16_t>& srcMinDisp, const cv::Mat_<int16_t> srcMaxDisp, int nrOfPixels) {
	int16_t min = srcMinDisp(y,x);
	int16_t max = srcMaxDisp(y,x);
	for (int i = 0; i < nrOfPixels; ++i) {
		if (baseImg.isValid(y, x - i)) extendRange(y, x - i, min, max);
		if (baseImg.isValid(y, x + i)) extendRange(y, x + i, min, max);
		if (baseImg.isValid(y - i, x)) extendRange(y - i, x, min, max);
		if (baseImg.isValid(y + i, x)) extendRange(y + i, x, min, max);
	}
}

inline bool DisparitySearchRange::extendRangeToMinDisp(int y, int x,
		int disp) {
	if (minDisp(y, x) > disp) {
		minDisp(y, x) = disp;
		return true;
	}
	return false;
}

inline bool DisparitySearchRange::extendRangeToMaxDisp(int y, int x,
		int disp) {
	if (maxDisp(y,x) < disp) {
		maxDisp(y,x) = disp;
		return true;
	}
	return false;
}

inline bool DisparitySearchRange::extendRange(int y, int x,
		int disp) {
	return extendRange(y, x, disp, disp+1);
}

inline bool DisparitySearchRange::extendRange(int y, int x,
		int min, int max) {
	bool result1 = extendRangeToMinDisp(y, x, min);
	bool result2 = extendRangeToMaxDisp(y, x, max);
	return result1 || result2;
}

void DisparitySearchRange::extendMinAndMaxDisparities(int threshold,
		int nrOfDisparites) {
	double min, max;
	cv::Mat_<uint8_t> mask;

	cv::minMaxLoc(minDisp, &min, &max);
	mask = minDisp < (int) (min) + (int) (threshold);
	cv::subtract(minDisp, nrOfDisparites, minDisp, mask);

	cv::minMaxLoc(maxDisp, &min, &max);
	mask = maxDisp > (int) (max) - (int) (threshold);
	cv::add(maxDisp, nrOfDisparites, maxDisp, mask);
}

void DisparitySearchRange::extendRangeToAllDirections(int nrOfPixels) {
	std::deque<int16_t> leftMin, rightMin, upperMin, lowerMin;
	std::deque<int16_t> leftMax, rightMax, upperMax, lowerMax;
	for (int y = 0; y < baseImg.getRows(); ++y) {
		leftMin.clear();
		leftMax.clear();
		for (int x = 1; x <= nrOfPixels; ++x) {
			rightMin.push_back(minDisp(y, x));
			rightMax.push_back(maxDisp(y, x));
		}
		for (int x = 0; x < baseImg.getCols(); ++x) {
			int16_t min = minDisp(y, x);
			int16_t max = maxDisp(y, x);
			if (leftMin.size() > 0) {
				int16_t lMin = *std::min_element(leftMin.begin(),
						leftMin.end());
				if (lMin < minDisp(y,x))
					minDisp(y,x) = lMin;
				int16_t lMax = *std::max_element(leftMax.begin(),
						leftMax.end());
				if (lMax > maxDisp(y,x))
					maxDisp(y,x) = lMax;
			}
			if (rightMin.size() > 0) {
				int16_t rMin = *std::min_element(rightMin.begin(),
						rightMin.end());
				if (rMin < minDisp(y,x))
					minDisp(y,x) = rMin;
				int16_t rMax = *std::max_element(rightMax.begin(),
						rightMax.end());
				if (rMax > maxDisp(y,x))
					maxDisp(y,x) = rMax;
			}

			leftMin.push_back(min);
			leftMax.push_back(max);
			if ((int)leftMin.size() > nrOfPixels) {
				leftMin.pop_front();
				leftMax.pop_front();
			}
			if (x + nrOfPixels + 1 < baseImg.getCols()) {
				rightMin.push_back(minDisp(y, x + nrOfPixels + 1));
				rightMax.push_back(maxDisp(y, x + nrOfPixels + 1));
			}
			if (rightMin.size() > 0) {
				rightMin.pop_front();
				rightMax.pop_front();
			}
		}
	}
	for (int x = 0; x < baseImg.getCols(); ++x) {
		upperMin.clear();
		upperMax.clear();
		for (int y = 1; y <= nrOfPixels; ++y) {
			lowerMin.push_back(minDisp(y, x));
			lowerMax.push_back(maxDisp(y, x));
		}
		for (int y = 0; y < baseImg.getRows(); ++y) {
			int16_t min = minDisp(y, x);
			int16_t max = maxDisp(y, x);
			if (upperMin.size() > 0) {
				int16_t uMin = *std::min_element(upperMin.begin(),
						upperMin.end());
				if (uMin < minDisp(y,x))
					minDisp(y,x) = uMin;
				int16_t uMax = *std::max_element(upperMax.begin(),
						upperMax.end());
				if (uMax > maxDisp(y,x))
					maxDisp(y,x) = uMax;
			}
			if (lowerMin.size() > 0) {
				int16_t lMin = *std::min_element(lowerMin.begin(),
						lowerMin.end());
				if (lMin < minDisp(y,x))
					minDisp(y,x) = lMin;
				int16_t lMax = *std::max_element(lowerMax.begin(),
						lowerMax.end());
				if (lMax > maxDisp(y,x))
					maxDisp(y,x) = lMax;
			}

			upperMin.push_back(min);
			upperMax.push_back(max);
			if ((int)upperMin.size() > nrOfPixels) {
				upperMin.pop_front();
				upperMax.pop_front();
			}
			if (y + nrOfPixels + 1 < baseImg.getRows()) {
				lowerMin.push_back(minDisp(y + nrOfPixels + 1, x));
				lowerMax.push_back(maxDisp(y + nrOfPixels + 1, x));
			}
			if (lowerMin.size() > 0) {
				lowerMin.pop_front();
				lowerMax.pop_front();
			}
		}
	}
}

void DisparitySearchRange::extendRangeTo(int minRange) {
	assert(minRange > 0);

	cv::Mat diff = maxDisp - minDisp;
	cv::Mat mask = (diff < minRange) & (diff > 0);
	cv::subtract(minDisp, minRange/2, minDisp, mask);
	cv::add(maxDisp, minRange/2, maxDisp, mask);
}

void DisparitySearchRange::writeMinMaxDisp(const std::string& nameSuffix) const {
	std::stringstream str;
	str << "debug-searchrange-minDisp-" << params.counter << nameSuffix << ".png";
	writeImg(str.str(), minDisp);
	str.str("");
	str << "debug-searchrange-maxDisp-" << params.counter << nameSuffix << ".png";
	writeImg(str.str(), maxDisp);
}

void DisparitySearchRange::writeImg(const std::string& name, const cv::Mat& img) const {
	assert(img.channels() == 1);

	cv::Mat pngImg, tmp;

	double min, max;
	cv::Point minPoint, maxPoint;
	tmp = img.clone();
	cv::minMaxLoc(tmp, &min, &max, &minPoint, &maxPoint, (img < img.cols) & (img > -img.cols));
	tmp.setTo(min, img > img.cols);
	tmp.setTo(max, img < -img.cols);

	cv::normalize(tmp, pngImg, 0, 255, cv::NORM_MINMAX, CV_8U);
	cv::Mat channels[] = { pngImg, pngImg, pngImg };
	cv::merge(channels, 3, pngImg);

	cv::circle(pngImg, minPoint, 3, cv::Scalar(0, 255, 0), 1);
	std::stringstream text;
	text << min << " at " << minPoint;
	int fontFace = cv::FONT_HERSHEY_PLAIN;
	double fontScale = 1;
	int thickness = 1;
	int baseline = 0;
	cv::Size size = cv::getTextSize(text.str(), fontFace, fontScale, thickness, &baseline);
	if (minPoint.x + size.width >= img.cols) minPoint.x = img.cols - size.width;
	if (minPoint.y < size.height) minPoint.y = size.height;
	cv::putText(pngImg, text.str(), minPoint, fontFace, fontScale, cv::Scalar(0,0,255), thickness);

	cv::circle(pngImg, maxPoint, 3, cv::Scalar(0, 255, 0), 1);
	text.str("");
	text << max << " at " << maxPoint;
	size = cv::getTextSize(text.str(), fontFace, fontScale, thickness, &baseline);
	if (maxPoint.x + size.width >= img.cols) maxPoint.x = img.cols - size.width;
	if (maxPoint.y < size.height) maxPoint.y = size.height;
	cv::putText(pngImg, text.str(), maxPoint, fontFace, fontScale, cv::Scalar(0,0,255), thickness);

	cv::imwrite(name, pngImg);
	if (params.debug) cv::imshow(name, pngImg);
}
