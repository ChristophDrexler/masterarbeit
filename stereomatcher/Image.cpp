/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include "Error.h"
#include "Image.h"

Image::Image(const std::string& filename, const std::string& maskFilename) {
	img = cv::imread(filename, cv::IMREAD_COLOR);
	if (maskFilename == "") {
		mask.create(img.size());
		mask.setTo(255);
	} else {
		mask = cv::imread(maskFilename, cv::IMREAD_GRAYSCALE);
	}
	if (img.size() != mask.size())
		throw Error("Size of the image " + filename + " is not the same as the size of the mask image " + maskFilename + "!");
}

const cv::Mat_<int16_t>& Image::getAddedIntensitiesMat() {
	if (addedIntensitiesMat.dims == 0)
		addIntensities();
	return addedIntensitiesMat;
}

const cv::Mat_<int16_t>& Image::getXGradientMat() {
	if (xGradientMat.dims == 0)
		computeXGradient();
	return xGradientMat;
}

const cv::Mat_<int16_t>& Image::getYGradientMat() {
	if (yGradientMat.dims == 0)computeYGradient();
	return yGradientMat;
}

void Image::addIntensities() {
	addedIntensitiesMat.create(img.size());
	std::vector<cv::Mat> channels;
	cv::split(img, channels);
	cv::add(channels[0], channels[1], addedIntensitiesMat, cv::noArray(), CV_16SC1);
	cv::add(addedIntensitiesMat, channels[2], addedIntensitiesMat, cv::noArray(), CV_16SC1);
}

void Image::computeXGradient() {
	xGradientMat.create(img.size());
	cv::Sobel(getAddedIntensitiesMat(), xGradientMat, CV_16S, 1, 0, 1, 1, 0, cv::BORDER_REFLECT);
}

void Image::computeYGradient() {
	yGradientMat.create(img.size());
	cv::Sobel(getAddedIntensitiesMat(), yGradientMat, CV_16S, 0, 1, 1, 1, 0, cv::BORDER_REFLECT);
}
