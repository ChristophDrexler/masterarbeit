/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef CENSUSFILTER_H_
#define CENSUSFILTER_H_

#include "CostFunction.h"

class CensusFilter : public CostFunction {
private:
	cv::Mat baseCensusOfAddedIntensities, matchCensusOfAddedIntensities;
	cv::Mat baseCensusOfYGradient, matchCensusOfYGradient;
	cv::Mat baseCensusMask, matchCensusMask;
public:
	CensusFilter(const Parameters& p,
			Image& baseImg,
			Image& matchImg,
			DisparitySpaceImage& costDSI) :
				CostFunction(p, baseImg, matchImg, costDSI) {}
	~CensusFilter() {}
public:
	void compute();
	void computeCensusOfAddedIntensities();
	void computeCensusOfYGradient();
	uint8_t compute(int y, int x, int disp);
	uint8_t computeCensusOfAddedIntensities(int y, int x, int disp);
	uint8_t computeCensusOfYGradient(int y, int x, int disp);
	uint8_t getInvalidCost() { return 255; }
private:
	void computeCensusImg(const cv::Mat_<int16_t>& src, cv::Mat& dest, cv::Mat& mask);
	void computeHammingDistance(const cv::Mat& baseCensusImg, const cv::Mat& matchCensusImg);
	uint8_t computeHammingDistance(uint8_t val1, uint8_t val2);
	uint8_t computeHammingDistanceMasked(uint8_t val1, uint8_t val2, uint8_t mask);
	uint8_t computeHammingDistance(uint16_t val1, uint16_t val2);
	uint8_t computeHammingDistanceMasked(uint16_t val1, uint16_t val2, uint16_t mask);
	uint8_t computeHammingDistance(uint32_t val1, uint32_t val2);
	uint8_t computeHammingDistanceMasked(uint32_t val1, uint32_t val2, uint32_t mask);
	uint8_t computeHammingDistance(uint64_t val1, uint64_t val2);
	uint8_t computeHammingDistanceMasked(uint64_t val1, uint64_t val2, uint64_t mask);
};

static const uint8_t countTable[] =
{
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
};

#endif /* CENSUSFILTER_H_ */
