/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef STEREOMATCHER_H_
#define STEREOMATCHER_H_

#include <memory>
#include <opencv2/opencv.hpp>

#include "Aggregation.h"
#include "CostFunction.h"
#include "DisparitySpaceImage.h"
#include "Evaluation.h"
#include "Image.h"
#include "Parameters.h"
#include "SupportRegion.h"

class StereoMatcher {
private:
	Parameters& params;
	Image& baseImg, matchImg;
	DisparitySpaceImage costDSI, aggregatedCostDSI;
public:
	StereoMatcher(Parameters& p,
			Image& baseImg,
			Image& matchImg) :
				params(p), baseImg(baseImg), matchImg(matchImg),
				costDSI(DisparitySpaceImage(params, baseImg)),
				aggregatedCostDSI(DisparitySpaceImage(params, baseImg)) {
		assert(params.minDisparity < params.maxDisparity);
	}
	void start();
	void startWithFixedSearchRange();
	void startWithSearchRangeEstimatedByFeatures();
private:
	std::auto_ptr<CostFunction> getCostFunction();
	std::auto_ptr<Aggregation> getAggregationMethod();
	void calculateConfidenceMasks(const cv::Mat_<int16_t> intDispImg,
			const cv::Mat_<float> floatDispImg, Evaluation& eval);
	void invalidateInsecurePixels();
	void markInsecurePixels(cv::Mat_<uint8_t>& invalidatedPixelsMask);
	void markPixelsAroundInsecurePixels(cv::Mat_<uint8_t>& invalidatedPixelsMask, int radius);
	void writeImg(const std::string& filename, const cv::Mat& img) const;
	void writeHistogram(const std::string& filename, const cv::Mat_<float>& img) const;
	void writeBaseAndMatchImg();
	void writeDisparityImages(cv::Mat_<uint8_t> validPixelsMask);
	void evaluateResult(Evaluation& evaluation, cv::Mat_<float> disparityImg,
			cv::Mat_<uint8_t> validPixelsMask);
	cv::Mat_<uint8_t> getValidPixelsMask();
};
#endif /* STEREOMATCHER_H_ */
