/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#ifndef SUPPORTREGION_H_
#define SUPPORTREGION_H_

#include <opencv2/opencv.hpp>

#include "Direction.h"
#include "Image.h"
#include "Parameters.h"

class SupportRegion {
private:
	const Parameters& params;
	const Image& img;
	cv::Mat_<uint8_t> leftArm, rightArm, upperArm, lowerArm;
public:
	SupportRegion(const Parameters& p, const Image& img) :
			params(p),
			img(img) {
		constructSupportRegion();
	}
	void constructSupportRegion();
	const cv::Mat_<uint8_t>& getRightArm() const { return rightArm; }
	const cv::Mat_<uint8_t>& getLeftArm()  const { return leftArm;  }
	const cv::Mat_<uint8_t>& getLowerArm() const { return lowerArm; }
	const cv::Mat_<uint8_t>& getUpperArm() const { return upperArm; }
private:
	uint8_t constructArm(Direction dir, int y, int x);
	void goToNextPoint(Direction dir, int& endpointY, int&endpointX);
	int getColorDistance(const cv::Vec3b& pixel, const cv::Vec3b& endpoint) const;
};

#endif /* SUPPORTREGION_H_ */
