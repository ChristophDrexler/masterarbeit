/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <iostream>
#include <vector>

#include "CensusFilter.h"
#include "Timer.h"

void CensusFilter::compute() {
	computeCensusOfAddedIntensities();
}

inline uint8_t CensusFilter::compute(int y, int x, int disp) {
	if (!baseImg.isValid(y, x) || !matchImg.isValid(y, x + disp))
		return getInvalidCost();
	return computeHammingDistanceMasked(
			baseCensusOfAddedIntensities.at<uint64_t>(y, x),
			matchCensusOfAddedIntensities.at<uint64_t>(y, x + disp),
			baseCensusMask.at<uint64_t>(y, x) & matchCensusMask.at<uint64_t>(y, x + disp)
			);
}

void CensusFilter::computeCensusOfAddedIntensities() {
	Timer timer;
	timer.start();

	matchImg.getAddedIntensitiesMat();

	if (baseCensusOfAddedIntensities.dims == 0)
		computeCensusImg(baseImg.getAddedIntensitiesMat(),
				baseCensusOfAddedIntensities, baseCensusMask);
	if (matchCensusOfAddedIntensities.dims == 0)
		computeCensusImg(matchImg.getAddedIntensitiesMat(),
				matchCensusOfAddedIntensities, matchCensusMask);

	computeHammingDistance(baseCensusOfAddedIntensities,
			matchCensusOfAddedIntensities);

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for computing the Census cost function: "
				<< timer.stop() << " s" << std::endl;
	if (params.verbosity & writeDebugImages)
		costDSI.writeWTAImage("WTA-Census.png");
}

void CensusFilter::computeCensusImg(const cv::Mat_<int16_t>& src, cv::Mat& dest,
		cv::Mat& mask) {
	assert(src.channels() == 1);

	dest.create(src.size(), CV_64FC1);
	mask.create(src.size(), CV_64FC1);
	int halfWindowWidth = (params.census.width - 1) / 2;
	int halfWindowHeight = (params.census.height - 1) / 2;
	int step = params.census.step;
	int startX, endX, startY, endY;
	for (int y = 0; y < src.rows; ++y) {
		startY = y - halfWindowHeight;
		endY = startY + params.census.height;
		for (int x = 0; x < src.cols; ++x) {
			uint64_t census = 0;
			startX = x - halfWindowWidth;
			endX = startX + params.census.width;
			int compVal;
			if (params.census.flagMCT) {
				int sum = 0, count = 0;
				for (int windowY = startY; windowY < endY; windowY += step) {
					for (int windowX = startX; windowX < endX; windowX +=
							step) {
						census *= 2;
						if (baseImg.isValid(windowY, windowX)) {
							++count;
							sum += src(windowY, windowX);
						}
					}
				}
				compVal = cvRound((double) sum / count);
			} else {
				compVal = src(y, x);
			}
			bool complete = true;
			for (int windowY = startY; windowY < endY; windowY += step) {
				for (int windowX = startX; windowX < endX; windowX += step) {
					if ((x != windowX) || (y != windowY)) {
						census *= 2;
						if (baseImg.isValid(windowY, windowX)) {
							if (src(windowY, windowX) > compVal)
								++census;
						} else {
							complete = false;
						}
					}
				}
			}
			dest.at<uint64_t>(y, x) = census;
			if (complete) {
				mask.at<uint64_t>(y, x) = 0xffffffffffffffff;
			} else {
				uint64_t maskVal = 0;
				for (int windowY = startY; windowY < endY; windowY += step) {
					for (int windowX = startX; windowX < endX; windowX += step) {
						if ((x != windowX) || (y != windowY)) {
							maskVal *= 2;
							if (baseImg.isValid(windowY, windowX))
								++maskVal;
						}
					}
				}
				mask.at<uint64_t>(y,x) = maskVal;
			}
		}
	}
}

void CensusFilter::computeHammingDistance(const cv::Mat& baseCensusImg,
		const cv::Mat& matchCensusImg) {
	assert(costDSI.getRows() == baseCensusImg.rows);
	assert(costDSI.getCols() == baseCensusImg.cols);
	assert(baseCensusImg.rows == matchCensusImg.rows);
	assert(costDSI.getMinDisparity() < costDSI.getMaxDisparity());
	assert(baseCensusImg.channels() == 1);
	assert(matchCensusImg.channels() == 1);
	assert(baseCensusImg.elemSize() == 8);
	assert(matchCensusImg.elemSize() == 8);

	cv::Mat_<uint8_t>& alreadyComputedPixels = costDSI.getComputedPixelsMat();
	for (int y = 0; y < baseCensusImg.rows; ++y) {
		for (int x = 0; x < baseCensusImg.cols; ++x) {
			if (alreadyComputedPixels(y, x) == 0) {
				DSIPixel& pixel = costDSI.at(y, x);
				if (pixel.isValid()) {
					uint64_t baseVal = baseCensusImg.at<uint64_t>(y, x);
					bool occlusion = false;
					for (int disp = pixel.getMinDisparity();
							disp < pixel.getMaxDisparity(); ++disp) {
						if (matchImg.isValid(y, x + disp)) {
							uint8_t cost = computeHammingDistance(baseVal,
									matchCensusImg.at<uint64_t>(y, x + disp));
							costDSI.at(y, x, disp) = std::min((int)cost, params.threshold1);
						} else {
							costDSI.at(y, x, disp) = costDSI.getMaxCost();
							occlusion = true;
						}
					}
					if (occlusion) {
						int sum = 0, count = 0;
						for (int disp = pixel.getMinDisparity();
								disp < pixel.getMaxDisparity(); ++disp) {
							if (costDSI.at(y, x, disp) < costDSI.getMaxCost()) {
								sum += costDSI.at(y, x, disp);
								++count;
							}
						}
						int mean =
								count > 0 ? sum / count : costDSI.getMaxCost();
						if (mean < pixel.getMaxCost()) {
							for (int disp = pixel.getMinDisparity();
									disp < pixel.getMaxDisparity(); ++disp) {
								if (costDSI.at(y, x, disp)
										== costDSI.getMaxCost())
									costDSI.at(y, x, disp) = mean;
							}
						}
					}
					alreadyComputedPixels(y, x) = 255;
				}
			}
		}
	}
}

inline uint8_t CensusFilter::computeHammingDistance(uint8_t val1, uint8_t val2) {
	return countTable[ val1 ^ val2 ];
}

inline uint8_t CensusFilter::computeHammingDistanceMasked(uint8_t val1,
		uint8_t val2, uint8_t mask) {
	return countTable[ (val1 ^ val2) & mask ];
}

inline uint8_t CensusFilter::computeHammingDistance(uint16_t val1, uint16_t val2) {
	uint16_t b = val1 ^ val2;
	uint8_t * p = (uint8_t*) &b;
	return countTable[p[0]] + countTable[p[1]];
}

inline uint8_t CensusFilter::computeHammingDistanceMasked(uint16_t val1,
		uint16_t val2, uint16_t mask) {
	uint16_t b = ( val1 ^ val2 ) & mask;
	uint8_t * p = (uint8_t*) &b;
	return countTable[p[0]] + countTable[p[1]];
}

inline uint8_t CensusFilter::computeHammingDistance(uint32_t val1, uint32_t val2) {
	uint32_t b = val1 ^ val2;
	uint8_t * p = (uint8_t*) &b;
	return countTable[p[0]] + countTable[p[1]] + countTable[p[2]]
			+ countTable[p[3]];
}

inline uint8_t CensusFilter::computeHammingDistanceMasked(uint32_t val1,
		uint32_t val2, uint32_t mask) {
	uint32_t b = ( val1 ^ val2 ) & mask;
	uint8_t * p = (uint8_t*) &b;
	return countTable[p[0]] + countTable[p[1]] + countTable[p[2]]
			+ countTable[p[3]];
}

inline uint8_t CensusFilter::computeHammingDistance(uint64_t val1, uint64_t val2) {
	uint64_t b = val1 ^ val2;
	uint8_t * p = (uint8_t*) &b;
	return countTable[p[0]] + countTable[p[1]] + countTable[p[2]]
			+ countTable[p[3]] + countTable[p[4]] + countTable[p[5]]
			+ countTable[p[6]] + countTable[p[7]];
}

inline uint8_t CensusFilter::computeHammingDistanceMasked(uint64_t val1,
		uint64_t val2, uint64_t mask) {
	uint64_t b = ( val1 ^ val2 ) & mask;
	uint8_t * p = (uint8_t*) &b;
	return countTable[p[0]] + countTable[p[1]] + countTable[p[2]]
			+ countTable[p[3]] + countTable[p[4]] + countTable[p[5]]
			+ countTable[p[6]] + countTable[p[7]];
}
