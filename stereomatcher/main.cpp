/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <getopt.h>
#include <iostream>
#include <sstream>
#include <string>
#include <opencv2/opencv.hpp>

#include "CensusFilter.h"
#include "Error.h"
#include "Parameters.h"
#include "StereoMatcher.h"
#include "Timer.h"

void printUsage() {
	std::cout << "Usage: ./stereomatcher [options] baseImageFile matchImageFile" << std::endl;
	std::cout << std::endl;
	std::cout << "Options:" << std::endl;
	std::cout << "-d <int>      | --min <int>                lower (inclusive) boundary for the disparities that should be taken into account - this disparity will be computed, too" << std::endl;
	std::cout << "-D <int>      | --max <int>                upper (exclusive) boundary for the disparities that should be taken into account - this disparity will *not* be computed" << std::endl;
	std::cout << "-v <int>      | --verbosity <int>          output verbosity flags which can be combined:" << std::endl;
	std::cout << "                                             0: noOutput" << std::endl;
	std::cout << "                                             1: writeBasicConsoleOutput" << std::endl;
	std::cout << "                                             2: writeTimesToConsole" << std::endl;
	std::cout << "                                             4: writeDebugConsoleOutput" << std::endl;
	std::cout << "                                            16: writeOnlyDisparityImage" << std::endl;
	std::cout << "                                            32: writeImportantImages" << std::endl;
	std::cout << "                                            64: writeDebugImages" << std::endl;
	std::cout << "                                           128: writeHistogramImages" << std::endl;
	std::cout << "-z <filename> | --baseValid <filename>     use the mask <filename> for determining which pixels of the base image are valid" << std::endl;
	std::cout << "-Z <filename> | --matchValid <filename>    use the mask <filename> for determining which pixels of the match image are valid" << std::endl;
	std::cout << "-f <filename> | --baseFeatures <filename>  use the file <filename> for the coordinates of the feature points in the base image" << std::endl;
	std::cout << "-F <filename> | --matchFeatures <filename> use the file <filename> for the coordinates of the feature points in the match image" << std::endl;
	std::cout << "-c <string>   | --cost <string>            cost function to use - only \"census\" is implemented in the release version (default: census)" << std::endl;
	std::cout << "-t <int>      | --threshold <int>          threshold to use for the cost function" << std::endl;
	std::cout << "-w <int>      | --censusWidth <int>        image width used for the census transform" << std::endl;
	std::cout << "-h <int>      | --censusHeight <int>       image height used for the census transform" << std::endl;
	std::cout << "-s <int>      | --censusStep <int>         step value used for the modified census transform" << std::endl;
	std::cout << "-m            | --censusMCT                use Modified Census Transform (MCT)" << std::endl;
	std::cout << "-a <string>   | --aggregation <string>     aggregation method to use - only \"sgm\" is implemented in the release version (default: sgm)" << std::endl;
	std::cout << "-p <int>      | --penalty1 <int>           penalty1 value used for SGM aggregation" << std::endl;
	std::cout << "-P <int>      | --penalty2 <int>           penalty2 value used for SGM aggregation" << std::endl;
	std::cout << "-x <filename> | --compare <filename>       compare the resulting disparity image to ground truth <filename>" << std::endl;
	std::cout << "-X <filename> | --mask <filename>          use this mask for the comparison with ground truth" << std::endl;
	std::cout << "-e <double>   | --threshold <double>       error threshold for disparity errors - used together with -X (default: 1.0)" << std::endl;
}

int main(int argc, char* argv[]) {
	assert(std::cout << "starting stereomatcher in debug mode" << std::endl);
	int c;
	bool okay = true;
	Parameters p;

	static struct option longOptions[] = {
		// basic parameters
		{"min",				required_argument, 0, 'd'},
		{"max",				required_argument, 0, 'D'},
		{"verbosity",		required_argument, 0, 'v'},
		{"baseValid",		required_argument, 0, 'z'},
		{"matchValid",		required_argument, 0, 'Z'},
		//features
		{"baseFeatures",	required_argument, 0, 'f'},
		{"matchFeatures",	required_argument, 0, 'F'},
		// cost functions
		{"cost",			required_argument, 0, 'c'},
		{"threshold",		required_argument, 0, 't'},
		{"censusWidth",		required_argument, 0, 'w'},
		{"censusHeight",	required_argument, 0, 'h'},
		{"censusStep",		required_argument, 0, 's'},
		{"censusMCT",		no_argument,       0, 'm'},
		// cost aggregation
		{"aggregation",		required_argument, 0, 'a'},
		{"penalty1",		required_argument, 0, 'p'},
		{"penalty2",		required_argument, 0, 'P'},
		// evaluation
		{"compare",			required_argument, 0, 'x'},
		{"mask",			required_argument, 0, 'X'},
		{"error",			required_argument, 0, 'e'},
        {0, 0, 0, 0}
	};

	while (true) {
		int optionIndex = 0;

		c = getopt_long(argc, argv, "d:D:v:z:Z:f:F:c:t:w:h:s:ma:p:P:x:X:e:", longOptions, &optionIndex);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 0:
			/* If this option set a flag, do nothing else now. */
			if (longOptions[optionIndex].flag != 0)
				break;
			std::cout << "option " << longOptions[optionIndex].name << std::endl;
			if (optarg)
				std::cout << " with arg " << optarg;
			std::cout << std::endl;
			break;

		case 'd':
			if (optarg) {
				std::istringstream(optarg) >> p.minDisparity;
			} else okay = false;
			break;

		case 'D':
			if (optarg) {
				std::istringstream(optarg) >> p.maxDisparity;
			} else okay = false;
			break;

		case 'v':
			if (optarg) {
				int value;
				std::istringstream(optarg) >> value;
				std::cout << "verbosity: ";
				if (value == noOutput) {
					p.verbosity = noOutput;
				}
				if (value & writeBasicConsoleOutput) {
					p.verbosity |= writeBasicConsoleOutput;
					std::cout << "writeBasicConsoleOutput ";
				}
				if (value & writeTimesToConsole) {
					p.verbosity |= writeTimesToConsole;
					std::cout << "writeTimesToConsole ";
				}
				if (value & writeDebugConsoleOutput) {
					p.verbosity |= writeDebugConsoleOutput;
					std::cout << "writeDebugConsoleOutput ";
				}
				if (value & writeOnlyDisparityImage) {
					p.verbosity |= writeOnlyDisparityImage;
					std::cout << "writeOnlyDisparityImage ";
				}
				if (value & writeImportantImages) {
					p.verbosity |= writeImportantImages;
					std::cout << "writeImportantImages ";
				}
				if (value & writeDebugImages) {
					p.verbosity |= writeDebugImages;
					std::cout << "writeDebugImages ";
				}
				if (value & writeHistogramImages) {
					p.verbosity |= writeHistogramImages;
					std::cout << "writeHistogramImages ";
				}
				std::cout << std::endl;
			}
			else okay = false;
			break;

		case 'z':
			if (optarg) {
				std::istringstream str(optarg);
				p.baseValidityFileName = str.str();
			} else okay = false;
			break;

		case 'Z':
			if (optarg) {
				std::istringstream str(optarg);
				p.matchValidityFileName = str.str();
			} else okay = false;
			break;

		case 'f':
			if (optarg) {
				std::istringstream str(optarg);
				p.baseFeaturesFileName = str.str();
			} else okay = false;
			break;

		case 'F':
			if (optarg) {
				std::istringstream str(optarg);
				p.matchFeaturesFileName = str.str();
			} else okay = false;
			break;

		case 'c':
			if (optarg) {
				std::istringstream str(optarg);
				if (str.str() == "ad")
					p.costFunction = functionAD;
				if (str.str() == "adcensus")
					p.costFunction = functionADCensus;
				if (str.str() == "adcensusgradient")
					p.costFunction = functionADCensusGradient;
				if (str.str() == "census")
					p.costFunction = functionCensus;
				if (str.str() == "censusofgradient")
					p.costFunction = functionCensusOfGradient;
				if (str.str() == "gradientcensus")
					p.costFunction = functionGradientCensus;
				if (str.str() == "gradientdiff")
					p.costFunction = functionGradientDiff;
			} else okay = false;
			break;

		case 't':
			if (optarg)
				std::istringstream(optarg) >> p.threshold1;
			else okay = false;
			break;

		case 'T':
			if (optarg)
				std::istringstream(optarg) >> p.threshold2;
			else okay = false;
			break;

		case 'w':
			if (optarg)
				std::istringstream(optarg) >> p.census.width;
			else okay = false;
			break;

		case 'h':
			if (optarg)
				std::istringstream(optarg) >> p.census.height;
			else okay = false;
			break;

		case 's':
			if (optarg)
				std::istringstream(optarg) >> p.census.step;
			else okay = false;
			break;

		case 'm':
			p.census.flagMCT = true;
			break;

		case 'a':
			if (optarg) {
				std::istringstream str(optarg);
				if (str.str() == "cross")
					p.aggregationMethod = aggregationCrossBased;
				if (str.str() == "crosssgm")
					p.aggregationMethod = aggregationCrossSGM;
				if (str.str() == "sgm")
					p.aggregationMethod = aggregationSGM;
			} else okay = false;
			break;

		case 'p':
			if (optarg)
				std::istringstream(optarg) >> p.sgm.penalty1;
			else okay = false;
			break;

		case 'P':
			if (optarg)
				std::istringstream(optarg) >> p.sgm.penalty2;
			else okay = false;
			break;

		case 'x':
			if (optarg) {
				std::istringstream str(optarg);
				p.groundTruthFileName = str.str();
			} else okay = false;
			break;

		case 'X':
			if (optarg) {
				std::istringstream str(optarg);
				p.maskFileNames.push_back(str.str());
			} else okay = false;
			break;

		case 'e':
			if (optarg) {
				std::istringstream(optarg) >> p.errorThreshold;
			}
			else okay = false;
			break;

		case '?':
			/* getopt_long already printed an error message. */
			break;

		default:
			printUsage();
			exit(EXIT_FAILURE);
		}
	}

	if (argc - optind != 2 || !okay) {
		printUsage();
		exit(EXIT_FAILURE);
	}
	if ( (p.minDisparity >= p.maxDisparity)
			&& (p.baseFeaturesFileName.size() == 0 || p.matchFeaturesFileName.size() == 0)) {
		std::cout << "You have to use either the options -f and -F, or -d and -D!" << std::endl;
		printUsage();
		exit(EXIT_FAILURE);
	}

	std::istringstream str(argv[optind++]);
	p.baseImgFileName = str.str();
	str.str(argv[optind++]);
	p.matchImgFileName = str.str();
	if (p.verbosity & writeDebugConsoleOutput) {
		std::cout << "base image: " << p.baseImgFileName << std::endl;
		std::cout << "match image: " << p.matchImgFileName << std::endl;
	}

	Timer timer;

	timer.start();
	Image baseImg(p.baseImgFileName, p.baseValidityFileName);
	Image matchImg(p.matchImgFileName, p.matchValidityFileName);

	StereoMatcher m(p, baseImg, matchImg);
	if ((p.baseFeaturesFileName != "" && p.matchFeaturesFileName != "")
			|| (p.minDisparity < p.maxDisparity)) {
		m.start();
	} else {
		printUsage();
		exit(EXIT_FAILURE);
	}

	if (p.verbosity & (writeBasicConsoleOutput | writeTimesToConsole | writeDebugConsoleOutput))
		std::cout << "Total duration: " << timer.stop() << " s" << std::endl;

	return EXIT_SUCCESS;
}
