/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

/*
 * For the definitions of confidence measures see:
 * 		HU, XIAOYAN ; MORDOHAI, PHILIPPOS:
 * 		A Quantitative Evaluation of Confidence Measures for Stereo Vision.
 * 		In: IEEE Transactions on Pattern Analysis and Machine Intelligence Preprint (2012-01-30), S. 1-14.
 * 		Online: http://dx.doi.org/10.1109/TPAMI.2012.46
 */

#include "Confidence.h"
#include "Timer.h"

double Confidence::getThreshold(const cv::Mat& confidenceValues, double minPercent, double maxPercent) const {
	assert(minPercent >= 0.0);
	assert(minPercent <= 100.0);
	assert(minPercent < maxPercent);

	Timer timer;
	timer.start();

	int count = 1000; // maximal number of iterations
	double floor, ceiling, percent, pixelNr, threshold;
	pixelNr = cv::countNonZero(dsi.getComputedValidPixelsMat());
	cv::Mat_<uint8_t> mask;
	cv::minMaxIdx(confidenceValues, &floor, &ceiling);
	do {
		threshold = ( floor + ceiling ) / 2;
		cv::compare(confidenceValues, threshold, mask, cv::CMP_GT);
		percent = cv::countNonZero(mask) * 100.0 / pixelNr;
		if (percent < minPercent)
			ceiling = threshold;
		if (percent > maxPercent)
			floor = threshold;
	} while (((percent < minPercent) || (percent > maxPercent)) && --count > 0);

	return threshold;
}

cv::Mat_<float> Confidence::getValuesLRC(const cv::Mat_<float>& baseDispImg,
		const cv::Mat_<int16_t>& matchDispImg) const {
	assert(baseDispImg.rows == matchDispImg.rows);

	Timer timer;
	timer.start();

	cv::Mat_<uint8_t> validMask = dsi.getComputedValidPixelsMat();
	cv::Mat_<float> lrcValues(baseDispImg.size());
	for (int y = 0; y < dsi.getRows(); ++y) {
		for (int x = 0; x < dsi.getCols(); ++x) {
			if (validMask(y,x) > 0) {
				if (x+baseDispImg(y,x) >= 0 && x+baseDispImg(y,x) < matchDispImg.rows) {
					lrcValues(y,x) = -std::abs(baseDispImg(y,x) + matchDispImg(y,x+baseDispImg(y,x)));
				} else lrcValues(y,x) = -1;
			} else {
				lrcValues(y,x) = -20;
			}
		}
	}
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for computing the LRC confidence image: "
				<< timer.stop() << " s" << std::endl;

	return lrcValues;
}

cv::Mat_<float> Confidence::getValuesMSM(const cv::Mat_<int16_t>& dispImg) const {
	Timer timer;
	timer.start();

	cv::Mat_<uint8_t> validMask = dsi.getComputedValidPixelsMat();
	cv::Mat_<uint16_t> costImg;
	cv::Mat_<float> msmValues;
	cv::Mat_<uint8_t> validDisparitiesMask;
	dsi.getCostAt(dispImg, costImg, validDisparitiesMask);
	costImg.convertTo(msmValues, CV_32F, -1);

	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for computing the MSM confidence image: "
				<< timer.stop() << " s" << std::endl;

	return msmValues;
}

cv::Mat_<float> Confidence::getValuesPKRN() const {
	Timer timer;
	timer.start();

	cv::Mat_<uint8_t> validMask = dsi.getComputedValidPixelsMat();
	cv::Mat_<float> pkrnValues(dsi.getRows(), dsi.getCols());
	pkrnValues = 0;
	uint16_t leastCost, secondLeastCost;
	for (int y = 0; y < dsi.getRows(); ++y) {
		for (int x = 0; x < dsi.getCols(); ++x) {
			if (validMask(y,x) > 0) {
				leastCost = secondLeastCost = dsi.valueAt(y, x, dsi.getMinDisparity());
				for (int disp = dsi.getMinDisparity() + 1; disp < dsi.getMaxDisparity(); ++disp) {
					uint16_t cost = dsi.valueAt(y, x, disp);
					if (cost <= leastCost) {
						secondLeastCost = leastCost;
						leastCost = cost;
					}
				}
				if (leastCost == 0)
					pkrnValues(y, x) = (float) secondLeastCost * 2;
				else
					pkrnValues(y, x) = (float) secondLeastCost / leastCost;
			}
		}
	}
	if (params.verbosity & writeTimesToConsole)
		std::cout << "Time for computing the PKRN confidence image: "
				<< timer.stop() << " s" << std::endl;
	return pkrnValues;
}
