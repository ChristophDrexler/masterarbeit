#!/bin/bash

function printUsage() {
  echo "Usage: stereomatcher <akronym> [fixed] [<options>]"
  echo "   <acronym> is one of tsukuba|venus|teddy|cones|clown_in_front|clown_sokrates|raum_off_axis|raum_poltergeist|sokrates_off_axis"
  echo "   fixed: use a fixed disparity range instead of disparity range estimation by using features"
  echo "   <options> means: additive options that should be passed to the stereomatcher program"
}

stereomatcher="$HOME/workspace/masterarbeit/stereomatcher_build/stereomatcher"
#verbosity="-v 127"

dir="$HOME/stereomatcher/$1"
[[ -d "$dir" ]] || { echo "Directory $dir does not exist!" ; printUsage; exit 1; }

groundTruth=$(ls $dir/disparity/rightRectDisp_*.exr 2>/dev/null)
[[ -e "$groundTruth" ]] || groundTruth=$(ls $dir/groundtruth*.png 2>/dev/null)
[[ -e "$groundTruth" ]] || { echo "Groundtruth file does not exist! $groundTruth" ; printUsage; exit 1; }
[[ -e "$groundTruth" ]] && groundTruth="-x $groundTruth"

case $1 in
  tsukuba) minDisp=-15; maxDisp=1;;
  venus) minDisp=-19; maxDisp=1;;
  teddy|cones) minDisp=-59; maxDisp=1;;
  *)
    offset=${groundTruth##*_offset_}
    offset=${offset%%_*}
    scale=${groundTruth##*_scale_}
    scale=${scale%%.exr}
    scale=${scale%%.png}
    minDisp=$(LANG=C printf %.0f $(echo "$offset - 0.5" | bc))
    maxDisp=$(LANG=C printf %.0f $(echo "scale=10; $offset + 1 / $scale + 1.5" | bc))
    ;;
esac

shift

if [[ $1 = "fixed" ]] ; then
  shift
else
  baseRectPoints="$dir/baseRectPoints.txt"
  [[ -e "$baseRectPoints" ]] || baseRectPoints="$dir/baseRectValidatedPoints.SURF.txt"
  [[ -e "$baseRectPoints" ]] || baseRectPoints=""
  [[ -e "$baseRectPoints" ]] && baseRectPoints="-f $baseRectPoints"

  matchRectPoints="$dir/matchRectPoints.txt"
  [[ -e "$matchRectPoints" ]] || matchRectPoints="$dir/matchRectValidatedPoints.SURF.txt"
  [[ -e "$matchRectPoints" ]] || matchRectPoints=""
  [[ -e "$matchRectPoints" ]] && matchRectPoints="-F $matchRectPoints"
fi

maskFilename="$dir/disparity/all.png"
[[ -e "$maskFilename" ]] && mask="-X $maskFilename"
maskFilename="$dir/all.png"
[[ -e "$maskFilename" ]] && mask="$mask -X $maskFilename"
maskFilename="$dir/disparity/nonocc.png"
[[ -e "$maskFilename" ]] && mask="$mask -X $maskFilename"
maskFilename="$dir/nonocc.png"
[[ -e "$maskFilename" ]] && mask="$mask -X $maskFilename"
maskFilename="$dir/disc.png"
[[ -e "$maskFilename" ]] && mask="$mask -X $maskFilename"

baseImg="$dir/rect_rechts_licht1.png"
[[ -e "$baseImg" ]] || baseImg="$dir/rechts_licht1.png"
[[ -e "$baseImg" ]] || baseImg="$dir/imL.png"
[[ -e "$baseImg" ]] || { echo "File $baseImg does not exist!" ; printUsage; exit 1; }

matchImg="$dir/rect_links_licht1.png"
[[ -e "$matchImg" ]] || matchImg="$dir/links_licht1.png"
[[ -e "$matchImg" ]] || matchImg="$dir/imR.png"
[[ -e "$matchImg" ]] || { echo "File $matchImg does not exist!" ; printUsage; exit 1; }

baseValidityMask="$dir/validRight.png"
[[ -e "$baseValidityMask" ]] || baseValidityMask=""
[[ -e "$baseValidityMask" ]] && baseValidityMask="-z $baseValidityMask"

matchValidityMask="$dir/validLeft.png"
[[ -e "$matchValidityMask" ]] || matchValidityMask=""
[[ -e "$matchValidityMask" ]] && matchValidityMask="-Z $matchValidityMask"

command="$stereomatcher $verbosity -d $minDisp -D $maxDisp $groundTruth $mask $baseValidityMask $matchValidityMask $baseRectPoints $matchRectPoints $baseImg $matchImg $*"

echo $command

$command
