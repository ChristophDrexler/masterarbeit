/*
 * This file is part of StereoMatcher.
 *
 * StereoMatcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * StereoMatcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with StereoMatcher.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * If you want to use, change and/or redistribute this software,
 * you can choose one of the following licenses:
 *
 * * the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Austria License,
 *   see http://creativecommons.org/licenses/by-nc-sa/3.0/at/
 *
 * * the GNU General Public License, either version 3 of the License, or any later version.
 *
 *
 * Copyright 2012 Christoph Drexler, Franz-Baumann-Weg 7, 6020 Innsbruck, Austria
 */

#include <iostream>
#include <opencv2/opencv.hpp>

#include "Error.h"
#include "Evaluation.h"
#include "Parameters.h"

void readme() {
	std::cout << " Usage:" << std::endl;
	std::cout << "./validate <baseRectPoints.txt>  <matchRectPoints.txt> <groundtruth_offset_0.0_scale_1.0.exr> to create the files baseRectValidatedPoints.txt and matchRectValidatedPoints.txt" << std::endl;
	std::cout << "./validate <baseRectPoints.txt>  <matchRectPoints.txt> <groundtruth_scale_-4.png> to create the files baseRectValidatedPoints.txt and matchRectValidatedPoints.txt" << std::endl;
}

int main(int argc, char** argv) {
	if (argc != 4) {
		readme();
		return -1;
	}

	Parameters params;
	params.groundTruthFileName = argv[3];
	Evaluation e(params);
	cv::Mat_<float> groundtruth = e.getGroundTruthImg();

	std::ifstream baseFeatures(argv[1]);
	std::ifstream matchFeatures(argv[2]);
	std::ofstream baseValidatedFeatures("baseRectValidatedPoints.txt");
	std::ofstream matchValidatedFeatures("matchRectValidatedPoints.txt");
	if (baseFeatures.fail() || matchFeatures.fail())
		throw Error("Error reading the feature coordinates!");
	while (baseFeatures.good() && matchFeatures.good()) {
		cv::Point2d basePoint, matchPoint;
		baseFeatures >> basePoint.x;
		baseFeatures >> basePoint.y;
		matchFeatures >> matchPoint.x;
		matchFeatures >> matchPoint.y;
		double disp = matchPoint.x - basePoint.x;
		double diff = std::abs(groundtruth.at<float>(basePoint) - disp);
		if (basePoint.x != 0 || basePoint.y != 0 || disp != 0) {
			if (diff <= 0.5) {
				std::cout << "feature ok: disparity " << disp << " at " << basePoint;
				std::cout << "; disparity in ground truth: " << groundtruth.at<float>(basePoint)
						<< std::endl;
				baseValidatedFeatures << basePoint.x << " ";
				baseValidatedFeatures << basePoint.y << std::endl;
				matchValidatedFeatures << matchPoint.x << " ";
				matchValidatedFeatures << matchPoint.y << std::endl;
			} else {
				std::cout << "discarding wrong feature: disparity " << disp << " at " << basePoint;
				std::cout << " instead of " << groundtruth.at<float>(basePoint)
						<< std::endl;
			}
		}
	}
}
